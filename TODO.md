# TODO

## Before releasing v0.1.0
 - [x] Implement pawn promotions in BoardCanvas
 - [x] Debug animations (have a more reliable approach (can segfault when clicking on variations in the editor))
 - [x] In BoardCanvas search for a workaround of the dynamic allocation of adata.buffer (on canvas resize)
 - [x] Bind the chess game editor settings to EditorPrefs.hpp
 - [x] Ask before closing MainWindow/Tabs if anything is not saved
 - [x] Disable the save button in GameTab after saving (and re-enable it on new changes)
 - [x] Make PGNGameBase use GotoNextGame() instead of ParseNextGame() in the NextGame() method to improve performance
 - [x] Clean and debug DragNDrop in BoardCanvas
 - [x] Disable the "Analyze entire game" button (Not Yet Implemented)
 - [x] Keep engine evaluation bar visible (and best move arrows) as long as the live engine dialog is open
 - [x] Debug engine name in the EngineTab configuration (use id for engine entry instead of name (otherwise name clash/bugs etc.))
 - [x] Use static libraries for dependencies
 - [x] Update PGNP to last commit
 - [x] Remove duplicated variations (currently if variation is played more than once it is added anyway to the move list)
 - [ ] Debug the Preference window on OS X (GetIcon() must be overridden)
 - [x] When MainWindow is resized, the chess move editor scroll get broken if it was in used(It is related to CGEditor, on canvas resize, ScrollX/ScrollY is not updated properly, thus item can become unreachable)
 - [ ] When OChess preferences are changed, they are not applied on the openned tabs (only on OS X)
 - [ ] In CGEditor, when moves are deleted, scroll mus be resets otherwise bugs (sounds like a reset scroll method in the Scrollbar should do the trick)
 
 ## Additional Features
 - [x] Add a live evaluation bar to the BoardCanvas
 - [x] Be able to draw arrows on the Board
 - [x] Highlight the last played move
 - [ ] Be able to play against an engine
 - [ ] Implement full chess engine game analyzer/annotator (analyze entire game)
 - [ ] Handle .si4 databases
 - [ ] Implement a page system for large databases (load massive databases per pages instead of entirely)
 - [ ] Add a tool in the toolbar of GameTabLeftPanel to run the live engine analysis using the last engine