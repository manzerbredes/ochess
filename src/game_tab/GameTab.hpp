#pragma once
#include "ChessArbiter.hpp"
#include "Game.hpp"
#include "HalfMove.hpp"
#include "left_panel/GameTabLeftPanel.hpp"
#include "right_panel/GameTabRightPanel.hpp"
#include "ochess.hpp"
#include <utility>
#include <wx/collpane.h>
#include <wx/splitter.h>
#include <wx/textctrl.h>
#include "right_panel/LiveEngineDialog.hpp"

wxDECLARE_EVENT(REFRESH_TAB_TITLE, wxCommandEvent);
wxDECLARE_EVENT(GAME_CHANGE, wxCommandEvent);
wxDECLARE_EVENT(SHOW_ENGINE_EVALUATION, wxCommandEvent);

/**
 * @brief Main tab for opened games. Contains GameTabLeftPanel and GameTabRightPanel.
 * @ingroup tabs
 */
class GameTab : public wxPanel, public TabInfos {
  GameTabRightPanel *editor_panel;
  GameTabLeftPanel *board_panel;
  std::shared_ptr<Game> game;
  std::string related_file;

  void RefreshLabel();
  void RefreshTabTitle();
  void OnGameChange(wxCommandEvent &event);
  void OnToolClick(wxCommandEvent &event);

public:
  GameTab(wxFrame *parent, std::shared_ptr<Game> game);
  void ApplyPreferences();
  std::shared_ptr<Game> GetGame() { return (std::shared_ptr<Game>(game)); }
  std::shared_ptr<GameBase> GetBase() { return nullptr; };
  void OnLink(){board_panel->SetSaveToolEnable(false);};
};
