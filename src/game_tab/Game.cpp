#include "Game.hpp"

Game::Game() : current(nullptr), moves(nullptr), result("*") {
  tags["White"] = "";
  tags["Black"] = "";
  initial_fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
  board = "rnbqkbnrpppppppp                                PPPPPPPPRNBQKBNR";
}

Game::Game(std::string fen) : current(nullptr), moves(nullptr), result("*") {
  tags["White"] = "";
  tags["Black"] = "";
  tags["FEN"] = fen;
  initial_fen = fen;
  board = chessarbiter::FENParser::Parse(fen).board;
}

Game::Game(HalfMove *m, std::string initial_fen) : result("*") {
  moves = m;
  current = m;
  this->initial_fen = initial_fen;
  board = chessarbiter::FENParser::Parse(initial_fen).board;
}

Game::Game(const Game* g){
  board=g->board;
  initial_fen=g->initial_fen;
  board = chessarbiter::FENParser::Parse(initial_fen).board;
  result=g->result;
  tags=g->tags;
  current=nullptr;
  moves=nullptr;
  if(g->moves != nullptr){
    moves=new HalfMove(g->moves);
  }
}

Game::~Game() {
  if (moves != nullptr) {
    delete moves;
  }
}

std::string Game::GetBoard() { return (board); }

std::string Game::GetTag(std::string tagname) { return (tags[tagname]); }

void Game::SetTag(std::string tagname, std::string value) {
  tags[tagname] = value;
}

bool Game::IsBlackToPlay() {
  if (current == nullptr) {
    return (false);
  }
  return (!current->IsBlack());
}

void Game::DeleteTag(std::string tagname) { tags.erase(tagname); }

void Game::DeleteMove(HalfMove *m) {
  if (moves == m) {
    current = nullptr;
    moves = nullptr;
    delete m;
  } else {
    if (m != nullptr) {
      current = static_cast<HalfMove*>(m->GetParent());
      if (current != nullptr) {
        current->RemoveChild(m);
      }
      delete m;
    }
  }
}

HalfMove *Game::GetCurrentMove() { return (current); }

HalfMove *Game::GetMoves() { return (moves); }

void Game::PromoteMove(HalfMove *m) {
  if (m != nullptr) {
    current = m;
    m->Promote();
  }
}

void Game::SetMoveAsMainline(HalfMove *m) {
  if (m != nullptr) {
    current = m;
    m->SetAsMainline();
  }
}

bool Game::IsCheckmate(bool forBlack){
  arbiter.Setup(GetFen());
  if(arbiter.IsCheckMate()){
    if(forBlack)
      return arbiter.IsBlackTurn();
    return !arbiter.IsBlackTurn();
  }
  return false;
}

bool Game::IsPromotionMove(std::string absolute_move){
  arbiter.Setup(GetFen());
  arbiter.Play(absolute_move);
  return(arbiter.WasPawnPromotion());
}

bool Game::Play(std::string move,char promotion) {
  wxLogDebug("Playing move %s", move);
  std::string fen = GetFen();
  arbiter.Setup(fen);
  if (arbiter.Play(move,promotion)) {
    HalfMove *m = new HalfMove(move, arbiter.GetSAN(), arbiter.GetFEN());
    char capture = arbiter.GetCapture();
    if (capture != ' ') {
      wxLogDebug("%c", capture);
      m->SetCapture(capture);
    }
    if (current != nullptr) {
      if(current->GetMainline()!=nullptr){
        HalfMove* curmainline=static_cast<HalfMove*>(current->GetMainline());
        HalfMove* movefound=curmainline->GetCurrentMoveWithFEN(arbiter.GetFEN());
        if(movefound!=nullptr){
          current=movefound;
        }
        else {
          current->AddMove(m);
          current = m;
        }
      }
      else{
        current->AddMove(m);
        current = m;
      }
    } else if (moves != nullptr) {
      moves->AddVariation(m);
      current = m;
    }
    if (moves == nullptr) {
      moves = m;
      current = m;
    }
    return (true);
  }
  return (false);
}

void Game::GetOpening(std::string &name,std::string &eco){
  HalfMove *m=current;
  if(m == nullptr)
    m=moves;
  if(m!=nullptr){
    // First check if opening already set
    std::string cname,ceco;
    m->GetOpening(cname,ceco);
    if(ceco.size()>0){
      name=cname;
      eco=ceco;
    }else {
      // If not, get the current move line (or first move)
      // and try to guess opening
      wxGetApp().GetBook().GuessOpening(m->GetLineAsSAN(),name,eco);
      m->SetOpening(name,eco);
    }
  }
}

void Game::Previous() {
  if (current != nullptr) {
    current = static_cast<HalfMove*>(current->GetParent());
  }
}

std::vector<std::string> Game::ListTags() {
  std::vector<std::string> keys;
  for (auto const &element : tags) {
    keys.push_back(element.first);
  }
  return (keys);
}

void Game::Next() {
  if (current != nullptr) {
    HalfMove *m = static_cast<HalfMove*>(current->GetMainline());
    if (m != nullptr) {
      current = m;
    }
  } else {
    current = moves;
  }
}

HalfMove *Game::GetNextMove(){
  if(current!=nullptr)
    return static_cast<HalfMove*>(current->GetMainline());
  return moves;
}

void Game::SetCurrent(HalfMove *m) { current = m; }

std::string Game::GetFen() {
  if (current == nullptr) {
    return (initial_fen);
  }
  return (current->GetFen());
}

std::string Game::GetResult() { return (result); }

void Game::SetResult(std::string result) { this->result = result; }

void Game::BuildAndVerify() {
  if (moves != nullptr) {
    moves->BuildAndVerify(GetFen());
  }
}
