#include <algorithm>

#include "../Game.hpp"
#include "LiveEngineDialog.hpp"
#include "editor/EditorCanvas.hpp"
#include "ochess.hpp"
#include <wx/listctrl.h>
#include <wx/notebook.h>

// Foreign events
wxDECLARE_EVENT(GAME_CHANGE, wxCommandEvent);
wxDECLARE_EVENT(SHOW_ENGINE_EVALUATION, wxCommandEvent);
wxDECLARE_EVENT(LIVE_ANALYSIS_STATUS, wxCommandEvent);

/**
 * @brief Right panel of the GameTab and contains the EditorCanvas and the live engine tab
 * 
 */
class GameTabRightPanel : public TabGameRightPanel {
  std::shared_ptr<Game> game;
  EditorCanvas *editor_canvas;
  long selected_item;
  LiveEngineDialog *live_engine;

public:
  GameTabRightPanel(wxFrame *parent, std::shared_ptr<Game> game);
  void Notify();
  void OnCommentChange(wxCommandEvent &event);
  void RefreshTagsList();
  void OnTagSelected(wxListEvent &event);
  void OnTagDeselected(wxListEvent &event);
  void OnApply(wxCommandEvent &event);
  void OnDelete(wxCommandEvent &event);
  void OnLiveAnalysis(wxCommandEvent &event);
  void ApplyPreferences();
  void OnLiveEngineClose(wxCloseEvent &e);
  std::uint8_t GetNagFromStr(std::string str);
};