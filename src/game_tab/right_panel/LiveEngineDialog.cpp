#include "LiveEngineDialog.hpp"

LiveEngineDialog::LiveEngineDialog(wxWindow *parent, std::uint32_t engine_id)
    : DialogLiveEngine(parent), engine(nullptr), interval(1000) {
  lines_list->InsertColumn(0, "#", wxLIST_FORMAT_LEFT, 50);
  lines_list->InsertColumn(1, "CP", wxLIST_FORMAT_LEFT, 70);
  lines_list->InsertColumn(2, "Line", wxLIST_FORMAT_LEFT, 300);

  // Load engine name
  confGroup="engines/"+std::to_string(engine_id);
  CONFIG_OPEN(conf);
  engine_name=conf->Read(confGroup+"/name");
  CONFIG_CLOSE(conf);
  current_engine->SetLabel(engine_name);

  InitEngine();
  Bind(wxEVT_BUTTON, &LiveEngineDialog::TogglePauseEngine, this,
       LIVE_ENGINE_PAUSE_BUTTON);
  Bind(wxEVT_CLOSE_WINDOW, &LiveEngineDialog::OnClose, this);
}

LiveEngineDialog::~LiveEngineDialog() {
  if (engine != nullptr) {
    wxLogDebug("LiveEngineDialog destructor: delete engine");
    delete engine;
  }
}

void LiveEngineDialog::InitEngine() {
  if (engine == nullptr) {
    wxLogDebug("Start engine: %s", engine_name);
    CONFIG_OPEN(conf);
    engine = new uciadapter::UCI(
        conf->Read(confGroup + "/path").ToStdString());
    engine->ucinewgame();

    long index;
    std::string optsPath = confGroup + "/options";
    conf->SetPath(optsPath);
    wxString opt_name;
    if (conf->GetFirstGroup(opt_name, index)) {
      do {
        wxString optPath = opt_name + "/";
        wxString default_value_wxString = conf->Read(optPath + "value");
        std::string default_value = default_value_wxString.ToStdString();
        engine->setoption(opt_name.ToStdString(), default_value);
        if (opt_name.Lower() == "multipv") {
          optmultipv=opt_name;
          multipv->SetValue(std::stoi(default_value_wxString.ToStdString()));
        } else if (opt_name.Lower() == "threads") {
          threads->SetLabel(default_value_wxString);
        }
      } while (conf->GetNextGroup(opt_name, index));
    }

    CONFIG_CLOSE(conf);
  }
  depth->Enable(false);
  multipv->Enable(false);
  timer.Start(interval);
  timer.Bind(wxEVT_TIMER, &LiveEngineDialog::OnTimerTick, this);
}

void LiveEngineDialog::OnClose(wxCloseEvent &e) {
  UNUSED(e);
  if (engine != nullptr) {
    wxLogDebug("Close live engine!!");
    timer.Stop();
    engine->stop();
    engine->quit();
  }
  Destroy();
}

void LiveEngineDialog::SetFEN(std::string fen) {
  if (timer.IsRunning()) {
    StopEngine();
    engine->position(fen);
    StartEngine();
  } else {
    engine->position(fen);
  }
}

void LiveEngineDialog::TogglePauseEngine(wxCommandEvent &event) {
  UNUSED(event);
  if (timer.IsRunning()) {
    StopEngine();
    engine_stop_button->SetLabel("Restart");
    depth->Enable(true);
    multipv->Enable(true);
  } else {
    engine_stop_button->SetLabel("Stop");
    depth->Enable(false);
    multipv->Enable(false);
    StartEngine();
  }
}

void LiveEngineDialog::StopEngine() {
  if (timer.IsRunning()) {
    timer.Stop();
  }
  engine->stop();
};

void LiveEngineDialog::StartEngine() {
  uciadapter::Go args;
  args.depth = depth->GetValue();
  if(optmultipv.size()>0)
    engine->setoption(optmultipv, std::to_string(multipv->GetValue()));
  engine->go(args);
  if (!timer.IsRunning()) {
    timer.Start(interval);
  }
}

void LiveEngineDialog::OnTimerTick(wxTimerEvent &event) {
  UNUSED(event);
  lines_list->DeleteAllItems(); // Clear lines_list
  engine->SyncAfter(0);
  EngineEvaluation *eval=new EngineEvaluation();
  auto lines=engine->GetLines(); // First best lines

  // First retrieve lines ids from unordered map lines
  std::vector<int> ids;
  for(auto const &line : lines){
    ids.push_back(line.first);
  }
  std::sort(ids.begin(),ids.end());
  if(ids.size()>0)
    eval->eval=lines[ids[0]].score_cp;
  // Now fill eval and lines_list
  for (int &id:ids) {
    auto const &line=lines[id];
    long index = lines_list->InsertItem(id, std::to_string(id));
    // Update eval that will be deplayed on the board
    if(line.pv.size()>0)
      eval->best_lines.push_back(line.pv[0]);
    // Refresh lines_list
    std::string line_moves;
    for (std::string move : line.pv)
      line_moves += move + " ";
    std::string cp_str = std::to_string(line.score_cp);
    if (line.score_cp > 0) {
      cp_str = "+" + cp_str;
    }
    lines_list->SetItem(index, 1, cp_str);
    lines_list->SetItem(index, 2, line_moves);
  }
  //wxLogDebug("%s", engine->GetBuffer());
  // Notify GameTab
  wxCommandEvent notifyEvent(SHOW_ENGINE_EVALUATION,GetId());
  notifyEvent.SetEventObject(this);
  notifyEvent.SetClientData(eval);
  ProcessEvent(notifyEvent);
}