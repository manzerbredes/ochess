#include "EditorCanvas.hpp"

EditorCanvas::EditorCanvas(wxFrame *parent, std::shared_ptr<Game> game)
    : wxPanel(parent), game(game) {
  hide_icon = LoadPNG("ui_eye_off", wxSize(CGEditor::status.MoveIconWidth,
                                     CGEditor::status.MoveIconWidth));
  t.ResizePieces(CGEditor::status.MoveIconWidth);
  
  // Theme:
  default_font=wxFont(*wxNORMAL_FONT).MakeBold();
  color_scrollbar_bg=wxColour("#FFFFFF");
  color_scrollbar=wxColour("#838383");
  color_margin=wxColour("#F3F3F3");
  color_comments_bg=wxColour("#ffffcc");
  color_current_move_bg=wxColour(216, 216, 216);
  color_menu_item_bg=wxColour(216, 216, 216);
  // The following should be called when using an EVT_PAINT handler
  SetBackgroundStyle(wxBG_STYLE_PAINT);
  
  Bind(wxEVT_KEY_DOWN, [p=this](wxKeyEvent &e){e.ResumePropagation(1);e.Skip();});
  Bind(wxEVT_KEY_UP, [p=this](wxKeyEvent &e){e.ResumePropagation(1);e.Skip();});
}

void EditorCanvas::OnPaint(wxPaintEvent &event) {
  UNUSED(event);
  wxPaintDC current_dc(this);
  current_dc.SetBackground(*wxWHITE_BRUSH);
  current_dc.Clear();
  dc = &current_dc;

  // Refresh canvas size
  wxSize sz = GetClientSize();
  CGEditor::status.CanvasWidth = sz.GetWidth();
  CGEditor::status.CanvasHeight = sz.GetHeight();

  const wxPoint pt = wxGetMousePosition();
  CGEditor::status.MouseX = pt.x - this->GetScreenPosition().x;
  CGEditor::status.MouseY = pt.y - this->GetScreenPosition().y;
  CGEditor::Draw();
}

/**
 * @brief Convenient fonction to center text
 *
 * @param e Element to center
 * @return wxPoint The centered version of e according to wdWidget API
 */
wxPoint EditorCanvas::Middle(cgeditor::Element e) {
  wxSize sz = dc->GetTextExtent(e.text);
  return (wxPoint(e.x + (e.width - sz.GetWidth()) / 2,
                  e.y + (e.height - sz.GetHeight()) / 2));
}

void EditorCanvas::DrawElement(const cgeditor::Element &e) {
  // TODO: Optimize brush!!!! Always instanciated at each call
  dc->SetPen(wxNullPen);
  dc->SetBrush(*wxRED_BRUSH);
  dc->SetFont(default_font);
  if (e.prop & cgeditor::Property::Rectangle) {
    if (e.prop & cgeditor::Property::Scrollbarbg) {
      dc->SetBrush(color_scrollbar_bg);
    } else if (e.prop & cgeditor::Property::Scrollbar) {
      dc->SetBrush(color_scrollbar);
    } else if (e.prop & cgeditor::Property::Margin) {
      dc->SetBrush(wxBrush(color_margin));
    } else if (e.prop & cgeditor::Property::Comment) {
      dc->SetBrush(wxBrush(color_comments_bg));
    } else if (e.prop & cgeditor::Property::Button) {
      if (e.prop & cgeditor::Property::On) {
        dc->DrawBitmap(hide_icon, e.x, e.y);
        return;
      }
      dc->SetBrush(*wxBLACK_BRUSH);
    }
    wxRect recToDraw(e.x, e.y, e.width, e.height);
    dc->DrawRectangle(recToDraw);
  } else if (e.prop & cgeditor::Property::Text ||
             e.prop & cgeditor::Property::Image) {
    if (e.prop & cgeditor::Property::Image) {
      // Draw your pieces images instead
      std::uint32_t y = Middle(e).y - CGEditor::status.MoveIconWidth / 2;
      char p = 'P';
      if (e.prop & cgeditor::Property::Knight) {
        p = 'N';
      } else if (e.prop & cgeditor::Property::Bishop) {
        p = 'B';
      } else if (e.prop & cgeditor::Property::Queen) {
        p = 'Q';
      } else if (e.prop & cgeditor::Property::King) {
        p = 'K';
      } else if (e.prop & cgeditor::Property::Rook) {
        p = 'R';
      }
      if (e.prop & cgeditor::Property::Black) {
        p = std::tolower(p);
      }
      if (e.prop & cgeditor::Property::Current) {
        wxRect recToDraw(e.x, e.y, e.width, e.height);
        dc->SetBrush(wxBrush(color_current_move_bg));
        dc->DrawRectangle(recToDraw);
      }
      dc->DrawBitmap(*t.Get(p), e.x, y);
    } else if (e.prop & cgeditor::Property::Comment) {
      dc->DrawText(wxString(e.text), wxPoint(e.x, e.y));
    } else if (e.prop & cgeditor::Property::Menuitem) {
      wxRect recToDraw(e.x, e.y, e.width, e.height);
      dc->SetBrush(wxBrush(color_menu_item_bg));
      dc->DrawRectangle(recToDraw);
      dc->DrawText(wxString(e.text), wxPoint(e.x, Middle(e).y));
    } else {
      std::string text=e.text;
      if (e.prop & cgeditor::Property::Move) {
        if (e.prop & cgeditor::Property::Current) {
          wxRect recToDraw(e.x, e.y, e.width, e.height);
          dc->SetBrush(wxBrush(color_current_move_bg));
          dc->DrawRectangle(recToDraw);
        }
        if(e.prop & cgeditor::Property::Nag){
          text=e.text;
        }
        // Draw move text
        if (CGEditor::status.UseMoveIcons) {
          dc->DrawText(wxString(text), wxPoint(e.x, Middle(e).y));
        } else {
          dc->DrawText(wxString(text), Middle(e));
        }
      } else {
        dc->DrawText(wxString(text), Middle(e));
      }
    }
  }
}
void EditorCanvas::HandleEvent(const cgeditor::Event &e) {
  if (e.type == cgeditor::Event::Goto) {
    game->SetCurrent((HalfMove *)e.move);
    SyncCache();
  } else if (e.type == cgeditor::Event::Delete) {
    game->DeleteMove((HalfMove *)e.move);
    SyncCache();
  } else if (e.type == cgeditor::Event::Promote) {
    game->PromoteMove((HalfMove *)e.move);
    SyncCache();
  } else if (e.type == cgeditor::Event::SetAsMainline) {
    game->SetMoveAsMainline((HalfMove *)e.move);
    SyncCache();   
  }
  wxCommandEvent event(GAME_CHANGE, GetId());
  event.SetEventObject(this);
  ProcessEvent(event);
}

void EditorCanvas::MouseEvent(wxMouseEvent &event) {
  if (event.Dragging()) {
    CGEditor::status.LeftClick = false;
    CGEditor::status.IsDrag = true;
    Refresh();
  } else if (event.LeftDown()) {
    SetFocus();
    CGEditor::status.LeftClick = true;
    Refresh();
  } else if (event.RightDown()) {
    SetFocus();
    CGEditor::status.RightClick = true;
    Refresh();
  } else if (event.GetWheelRotation() != 0) {
    SetFocus();
    if (event.GetWheelRotation() < 0) {
      CGEditor::status.EventVScroll = 50;
    } else {
      CGEditor::status.EventVScroll = -50;
    }
    Refresh();
  }

  // Handle editor events
  Update();
  if(ProcessEvents()){
    Refresh();
    Update();
  }
}

void EditorCanvas::SetMoves(HalfMove *moves, HalfMove *current) {
  CGEditor::status.Moves = moves;
  CGEditor::status.CurrentMove = current;
  // Focus on current move:
  if(current!=nullptr){
    CGEditor::FocusOnMove(current);
  }
  Refresh();
}

void EditorCanvas::ApplyPreferences(){
  CONFIG_OPEN(conf);
  conf->SetPath("editor/");
  CGEditor::status.MoveHeight=conf->Read("row_size", 50);
  CGEditor::status.MoveWidth=conf->Read("col_size", 100);
  CGEditor::status.UseMoveIcons=conf->Read("show_move_icons", true);
  color_margin=wxColour(conf->Read("color_margin", "#F3F3F3"));
  color_scrollbar=wxColour(conf->Read("color_scrollbar", "#838383"));
  color_scrollbar_bg=wxColour(conf->Read("color_scrollbarbg", "#FFFFFF"));
  color_comments_bg=wxColour(conf->Read("color_commentbg", "#ffffcc"));
  CONFIG_CLOSE(conf);
  Refresh();
}

wxBEGIN_EVENT_TABLE(EditorCanvas, wxPanel) EVT_PAINT(EditorCanvas::OnPaint)
    EVT_MOUSE_EVENTS(EditorCanvas::MouseEvent) wxEND_EVENT_TABLE()
