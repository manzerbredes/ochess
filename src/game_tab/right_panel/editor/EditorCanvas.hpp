#pragma once

#include "../../HalfMove.hpp"
#include "CGEditor.hpp"
#include "ochess.hpp"
#include "../../left_panel/board/Theme.hpp"
#include "game_tab/Game.hpp"

// Foreign events
wxDECLARE_EVENT(GAME_CHANGE, wxCommandEvent);

/**
 * @brief Contains the moves editor for the currently opened game
 * 
 */
class EditorCanvas : public wxPanel, public cgeditor::CGEditor {
  wxPaintDC *dc;
  wxPoint Middle(cgeditor::Element e);
  wxBitmap hide_icon;
  Theme t;
  wxFont default_font;
  std::shared_ptr<Game> game;

  wxColour color_scrollbar_bg;
  wxColour color_scrollbar;
  wxColour color_margin;
  wxColour color_comments_bg;
  wxColour color_current_move_bg;
  wxColour color_menu_item_bg;

public:
  EditorCanvas(wxFrame *parent, std::shared_ptr<Game> game);
  void OnPaint(wxPaintEvent &event);
  void MouseEvent(wxMouseEvent &event);
  void DrawElement(const cgeditor::Element &e);
  void HandleEvent(const cgeditor::Event &e);
  void SetMoves(HalfMove *moves, HalfMove *current);
  void ApplyPreferences();
  DECLARE_EVENT_TABLE()
};
