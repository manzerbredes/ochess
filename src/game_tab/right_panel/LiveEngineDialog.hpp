#pragma once

#include "UCI.hpp"
#include "ochess.hpp"
#include <wx/timer.h>
#include <algorithm>

wxDECLARE_EVENT(SHOW_ENGINE_EVALUATION, wxCommandEvent);

/// @brief Contains the current engine evaluation (sorted vector of best lines + position score in cp)
typedef struct EngineEvaluation {
  std::vector<std::string> best_lines;
  float eval=0;
} EngineEvaluation;

/**
 * @brief Dialog to control the current running engine on the game tab
 * 
 */
class LiveEngineDialog : public DialogLiveEngine {
  uciadapter::UCI *engine;
  std::string engine_name;
  std::string confGroup;
  std::string optmultipv;

  wxTimer timer;
  /// @brief The following time interval definitely need to be configure in the user settings (set to 1s for now)
  std::uint32_t interval;
  /**
   * @brief Called to fetch last evaluation from the engine subprocess (stockfish, fritz etc.)
   * 
   * @param event 
   */
  void OnTimerTick(wxTimerEvent &event);
  void OnClose(wxCloseEvent &e);
  /**
   * @brief Create the engine sub process using the uciadapter library
   * 
   */
  void InitEngine();
  /// @brief Pause/Resume evaluation
  void TogglePauseEngine(wxCommandEvent &event);
public:
  LiveEngineDialog(wxWindow *parent, std::uint32_t engine_id);
  ~LiveEngineDialog();
  /**
   * @brief Used setup a new position to evaluate
   * 
   * @param fen position to evaluate
   */
  void SetFEN(std::string fen);
  void StopEngine();
  void StartEngine();
};