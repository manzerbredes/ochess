#pragma once
#include "CMI.hpp"
#include "ChessArbiter.hpp"
#include "ochess.hpp"
#include "pgnp.hpp"
#include <map>
#include <vector>

/**
 * @brief This class extends CGEHalfMove (to be displayed in the game editor)
 * @ingroup game
 */
class HalfMove : public CMI::HalfMove {
  std::string fen;
  /// @brief Used in to retrieve captured pieces (see GetLineCaptures())
  char capture;
  void BuildAndVerify(HalfMove *m, std::string fen);
  /// @brief Store the source and destination square of the current move (mainly used for pieces animation)
  std::string src,dst;
  /// @brief Opening reach by that move while taking into account all the parents
  std::string opening, eco;
  /// @brief Arbiter used to ensure that chess rules are followed
  chessarbiter::ChessArbiter arbiter;

public:
  HalfMove(HalfMove *m);
  HalfMove(std::string move_absolute,std::string move_san);
  HalfMove(std::string move_absolute,std::string move_san, std::string fen);
  HalfMove(CMI::HalfMove *m);

  /**
   * @brief Add mainline to the current move.
   * If a mainline already exists, add to its variations
   * 
   * @param m the move to add
   */
  void AddMove(HalfMove *m);
  /// @brief Check if current half move is within a variation
  bool IsVariation();
  /**
   * @brief Check if pointer @a m to a HalfMove is a parent of the current one
   * 
   * @param m 
   * @return true 
   * @return false 
   */
  bool HasParent(HalfMove*m);
  /**
   * @brief Check if a given pointer @a m to a HalfMove is in mainline or variations of the current move.
   * 
   * @param m 
   * @return true 
   * @return false 
   */
  bool HasChild(HalfMove*m);
  /// @brief Retrieve the list of moves from the current one to the first one
  std::vector<HalfMove *> GetLine();
  std::string GetLineAsSAN();
  std::map<char, std::uint8_t> GetLineCaptures();
  /// @brief The opening name of current line
  void SetOpening(const std::string &name, const std::string &eco);
  /// @brief Getters for name and eco
  void GetOpening(std::string &name, std::string &eco);
  std::string GetFen();
  void SetFen(std::string fen);
  void SetCapture(char c);
  void GetAbsoluteMove(std::string &src,std::string &dst);
  void SetAbsoluteMove(const std::string &move_absolute);
  /**
   * @brief Search if current move (move or its variations)
   * contains a specific fen (usefull to not add moves that already exist)
   * @param fen 
   * @return HalfMove* the move if found or nullptr
   */
  HalfMove* GetCurrentMoveWithFEN(const std::string fen);
  /**
   * @brief Build current move
   * Verify and play all the moves in the game
   * while building the fen for each move
   */
  void BuildAndVerify(std::string initial_fen);
};
