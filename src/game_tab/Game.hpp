#pragma once

#include "ChessArbiter.hpp"
#include "HalfMove.hpp"
#include "ochess.hpp"
#include <unordered_map>

/**
 * @brief Hold an entire chess game. Used in many places in the projects.
 * @ingroup game
 */
class Game {
  /// @brief 64 char string that contains all the pieces on the board (used in BoardCanvas)
  HalfMove *current;
  HalfMove *moves;
  std::string board;
  std::string initial_fen;
  std::string result;
  std::unordered_map<std::string, std::string> tags;
  /// @brief Used by various methods of the class
  chessarbiter::ChessArbiter arbiter;

public:
  Game(const Game* g);
  Game();
  Game(std::string fen);
  Game(HalfMove *m, std::string initial_fen);
  ~Game();
  std::string GetBoard();
  std::string GetTag(std::string tagname);
  void SetTag(std::string tagname, std::string value);
  void DeleteTag(std::string tagname);
  HalfMove *GetCurrentMove();
  HalfMove *GetNextMove();
  HalfMove *GetMoves();
  std::string GetFen();
  std::string GetResult();
  void GetOpening(std::string &name,std::string &eco);
  /// @brief Play the given absolute move
  bool Play(std::string move,char promotion='q');
  bool IsBlackToPlay();
  bool IsCheckmate(bool forBlack);
  /// @brief Check if a given absolute move consists in a pawn promotion
  bool IsPromotionMove(std::string absolute_move);
  void Previous();
  void Next();
  /// @brief Delete a move (its mainline and variations recursively)
  void DeleteMove(HalfMove *m);
  void PromoteMove(HalfMove *m);
  void SetMoveAsMainline(HalfMove *m);
  void SetCurrent(HalfMove *m);
  std::vector<std::string> ListTags();
  void SetResult(std::string result);
  /**
   * @brief Build current game
   * Verify and play all the moves in the game
   * while building the fen for each move
   */
  void BuildAndVerify();
};