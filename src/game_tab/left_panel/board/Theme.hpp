#pragma once

#include "ochess.hpp"

#include <string>
#include <unordered_map>

#define ELT_DIM 200
#define DEFAULT_SIZE 80
#define PIECE_SIZE_FACTOR 0.8 // Should be between 0 and 1
#define MAT_SIZE_FACTOR 0.4
#define DEFAULT_PIECE_THEME "assets/pieces/cburnett.png"
#define DEFAULT_SQUARE_THEME "assets/boards/chesscom_8bits.png"

/**
 * @brief The in memory board theme (used by BoardCanvas)
 * 
 */
class Theme {
private:
  std::unordered_map<char, wxImage> skin;
  std::unordered_map<char, wxBitmap *> skin_scaled;
  std::uint8_t square_radius;
  wxMask *RoundedMask(std::uint32_t width, std::uint8_t corner);

public:
  Theme();
  /// @brief Create piece using two png file path
  Theme(std::string piece, std::string square);
  ~Theme();
  /// @brief Load piece skin image (break image tile into individual pieces)
  void LoadPiecesSkin(wxImage skin);
  /// @brief Load square skin image (break the 2 square tiles into individual squares)
  void LoadSquaresSkin(wxImage iskin);
  /// @brief Set pieces width
  void ResizePieces(std::uint32_t width);
  /// @brief Set squares width
  void ResizeSquares(std::uint32_t width);
  /// @brief Set square width and adjust piece size accordingly
  void ResizeSquaresAndPieces(std::uint32_t width);
  /// @brief Having rounded corners on squares
  void SetSquareRadius(std::uint8_t radius);
  std::uint8_t GetSquareRadius();
  bool Zoom(int amount);
  double GetPiecesSizes();
  double GetSquaresSizes();
  /**
   * @brief Get bitmap of an element
   * 
   * @param c For black pieces rnbkqp for white pieces RNBKQP and # for mate symbol and s for black square and S for white square
   * @return wxBitmap* 
   */
  wxBitmap *Get(char c);
};
