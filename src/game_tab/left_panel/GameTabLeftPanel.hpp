#pragma once

#include "../Game.hpp"
#include "board/BoardCanvas.hpp"
#include "ochess.hpp"
#include "base_tab/gamebase/GameBase.hpp"
#include "game_tab/right_panel/LiveEngineDialog.hpp"

// Foreign events
wxDECLARE_EVENT(GAME_CHANGE, wxCommandEvent);

/**
 * @brief Panel that contains the BoardCanvas and the bottom control buttons
 * 
 */
class GameTabLeftPanel : public TabGameLeftPanel {
  std::shared_ptr<Game> game;
  BoardCanvas *board_canvas;
  bool repeat;
  HalfMove *last_move;
  std::vector<GameState::Arrow> engine_arrows;
  std::string promote_on;
  std::string promotion_move;
  float eval_cp;
  bool is_engine_on;

public:
  GameTabLeftPanel(wxFrame *parent, std::shared_ptr<Game> game);
  void Notify(bool skip_animation=false);
  void OnPlay(wxCommandEvent &event);
  void OnGotoMove(wxCommandEvent &event);
  void OnRefreshBoard(wxCommandEvent &event);
  void ApplyPreferences();
  void SetSaveToolEnable(bool state){game_toolbar->EnableTool(0,state);};
  void SetEngineEvaluation(EngineEvaluation eval);
  void SetLiveEngineState(bool isOn);
};