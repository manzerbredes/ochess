#pragma once

#include "base_tab/BaseTab.hpp"
#include "game_tab/GameTab.hpp"
#include <wx/aui/auibook.h>
#include <wx/filedlg.h>
#include <wx/preferences.h>
#include <wx/textdlg.h>
#include "config.h"
#include "binres/binres.hpp"

wxDECLARE_EVENT(REFRESH_TAB_TITLE, wxCommandEvent);
wxDECLARE_EVENT(NEW_GAME_EVENT, wxCommandEvent);
wxDECLARE_EVENT(CLOSE_TAB_EVENT, wxCommandEvent);
wxDECLARE_EVENT(REFRESH_ENGINE_LIST, wxCommandEvent);
wxDECLARE_EVENT(CLOSE_LINKED_TAB, wxCommandEvent);

/**
 * @brief Main GUI window of OChess
 * 
 */
class MainWindow : public MainFrame {
  /// @brief Preference popup window
  wxPreferencesEditor *prefsEditor;
  /// @brief Menu to manage existing chess engines
  wxMenu *manageMenu;
  /// @brief Store the number of available chess engines (to create entries in the menus)
  int engine_count;

  void OnClose(wxCloseEvent &e);
  void NewGame(bool useFen);
  void OnPageChange(wxAuiNotebookEvent &event);
  void OnRefreshTabTitle(wxCommandEvent &event);
  void OpenSettings();
  /// @brief Setting up a new chess engine (select executable and open engine tab)
  void NewEngine();
  void OnCloseTabEvent(wxCommandEvent &event);
  void OnRefreshEngineList(wxCommandEvent &event);
  void OnMenuItemClick(wxCommandEvent &event);
  void OnAuiNotebookPageClosed(wxAuiNotebookEvent& event);
  void OnAuiNotebookPageCheck(wxAuiNotebookEvent& event);
  void OnCloseTabLinkedTo(wxCommandEvent &event);
  void AddPage(wxWindow* window, TabInfos* infos);
  void ShowAbout();
public:
  MainWindow();
  TabInfos* NewGame(std::shared_ptr<Game> game);
  /// @brief Apply preferences to MainWindow and all the opened tabs
  void ApplyPreferences();
};