#pragma once

#include "binres/openings.hpp"
#include "pgnp.hpp"

/**
 * @brief Guess the opening using the Lichess Opening Database
 * See: https://github.com/lichess-org/chess-openings
 */
class Openings {
    /// @brief Loaded tsv data format as a vector of tuples (<eco>,<name>,<pgn-moves-list>)
    typedef std::vector<std::tuple<std::string,std::string,std::string>> Volume;

    Volume A,B,C,D,E;
    /**
     * @brief Search opening name an ECO code based on the given \a moves
     * 
     * @param moves Half moves that you want to search for the opening
     * @param name Fill by the method if opening is found
     * @param eco  Fill by the method if opening is found
     */
    void SearchOpening(const pgnp::HalfMove *moves,std::string &name, std::string &eco);
    /**
     * @brief Load a volume using tsv data (see openings.hpp)
     * 
     * @param tsv data to load
     * @param vol volume in which the data will be loaded
     */
    void LoadVolume(const std::string &tsv, Volume *vol);
public:
    /**
    * @brief Guess the opening based on a list of SAN moves (PGN)
    * 
    * @param SANMoves 
    * @param name 
    * @param eco 
    */
    void GuessOpening(const std::string &SANMoves, std::string &name, std::string &eco);
    /**
     * @brief Guess the opening based on a half moves (wrapper around ::SearchOpening)
     * 
     * @param moves 
     * @param name 
     * @param eco 
     */
    void GuessOpening(const pgnp::HalfMove *moves, std::string &name, std::string &eco);
    Openings();
};