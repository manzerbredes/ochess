#include "binres.hpp"
#include <unordered_map>

// Embedded binary data (e.g: PGNs icons):
#include "binary_data.hpp"

wxBitmap LoadPNG(std::string icon, wxSize size) {
  wxImage img = LoadPNG(icon).ConvertToImage();
  return (wxBitmap(
      img.Scale(size.GetWidth(), size.GetHeight(), wxIMAGE_QUALITY_HIGH)));
}

wxBitmap LoadPNG(std::string icon) {
  std::unordered_map<std::string, wxBitmap> u = {
    {"ui_zoom_in", wxBITMAP_PNG_FROM_DATA(ui_zoom_in)},
    {"ui_zoom_out", wxBITMAP_PNG_FROM_DATA(ui_zoom_out)},
    {"ui_coins_swap", wxBITMAP_PNG_FROM_DATA(ui_coins_swap)},
    {"ui_eye_off", wxBITMAP_PNG_FROM_DATA(ui_eye_off)},
    {"ui_copy", wxBITMAP_PNG_FROM_DATA(ui_copy)},
    {"ui_save_floppy_disk", wxBITMAP_PNG_FROM_DATA(ui_save_floppy_disk)},
    {"ui_rook", wxBITMAP_PNG_FROM_DATA(ui_rook)},
    {"ui_text_alt", wxBITMAP_PNG_FROM_DATA(ui_text_alt)},
    {"mat", wxBITMAP_PNG_FROM_DATA(mat)},
    {"ochess", wxBITMAP_PNG_FROM_DATA(ochess)},
    {"cburnett", wxBITMAP_PNG_FROM_DATA(cburnett)},
    {"chesscom_8bits", wxBITMAP_PNG_FROM_DATA(chesscom_8bits)}
  };
  // Return png if exists
  if(u.count(icon)){
    return u[icon];
  }
  // Otherwise null bitmap
  return (wxNullBitmap);
}

