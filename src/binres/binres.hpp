/**
 * @file binres.hpp
 * @author Manzerbredes
 * @brief Binary resources functions
 * @version 0.1
 * @date 2023-05-12
 * 
 * @copyright Copyright (c) 2023
 */
#pragma once

#include "ochess.hpp"

/**
 * @brief Load an icon from embedded binary resources and rescale it
 * to a specified @a size
 *
 * @param icon The icon name
 * @param size Scale the icon to the specified size
 * @return wxBitmap 
 * @ingroup binres
 */
wxBitmap LoadPNG(std::string icon, wxSize size);

/**
 * @brief Load an icon from embedded binary resources
 * 
 * @param icon 
 * @return wxBitmap the image or wxNullBitmap if not found
 * @ingroup binres
 */
wxBitmap LoadPNG(std::string icon);

