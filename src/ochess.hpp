#pragma once

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "binres/binres.hpp"
#include "Openings.hpp"
#include "gui.h"
#include <memory>
#include <wx/app.h>
#include <wx/config.h>
#include <wx/filefn.h> // Check file exists etc
#include <wx/log.h>
#include <wx/busyinfo.h>
#include <vector>

#define MAINWIN ((MainWindow *)wxGetApp().GetTopWindow())

#define SHOW_DIALOG_ERROR(message)                                             \
  {                                                                            \
    wxMessageDialog *dial = new wxMessageDialog(                               \
        NULL, wxT(message), wxT("Error"), wxOK | wxICON_ERROR);                \
    dial->ShowModal();                                                         \
  }
#define SHOW_DIALOG_INFO(message) {wxMessageBox( wxT(message) );}
#define SHOW_DIALOG_BUSY(message) {wxBusyInfo wait(message);}
#define REQUIRE_FILE(file)                                                     \
  {                                                                            \
    if (!wxFileExists(file)) {                                                 \
      Abort(std::string("File ") + file + std::string(" not found"));          \
    }                                                                          \
  }

#define CONFIG_OPEN(name) wxConfig *name = new wxConfig("ochess")
#define CONFIG_CLOSE(name) delete name
#define CONFIG_VERSION "1.0"
#define UNUSED(symbol) (void)(symbol);

class Game;
class GameBase;
/**
 * @brief Used by each tab of the GUI to attach additional informations and features
 *
 */
class TabInfos {
  /// @brief Keep track of the number of opened tabs 
  static long tab_count;
public:
  /// @brief Which type of tab is it?
  typedef enum Type { GAME, BASE, ENGINE, NONE } Type;
  Type type;
  /// @brief Each tab has an associated unique id
  long id;
  /// @brief Specify to which tab id this tab is linked (e.g: database to linked to on of its opened game tab)
  long linked_id;
  /// @brief Set to true if this tab is attach to another one (c.f linked_id)
  bool is_linked;
  /// @brief True if current tab is dirty
  bool is_dirty;
  TabInfos(Type type_) : type(type_), id(tab_count), is_linked(false), is_dirty(false) { tab_count++; }
  void Link(TabInfos *tab);
  virtual void Refresh(){};
  /// @brief Callback that is called when the current tab is linked to another one
  virtual void OnLink(){};
  /// @brief Can be called to load preferences that have been modify in the application settings
  virtual void ApplyPreferences() {};
  virtual std::shared_ptr<Game> GetGame() = 0;
  virtual std::shared_ptr<GameBase> GetBase() = 0;
  virtual std::uint32_t GetEngineId() { return 0; };
};


/**
 * @brief Main application
 *
 */
class MyApp : public wxApp {
public:
  Openings Book;
  /// @brief Entry point of the application
  virtual bool OnInit();
  /// @brief Get a list of all the tabs opened in OChess
  std::vector<TabInfos *> ListTabInfos();
  /// @brief Trigger a wxWidget focus event on a specific tab
  void FocusOnTab(TabInfos *);
  /// @brief Open a new game tab linked to @a tabsrc that uses game @a g
  void NewGame(TabInfos *tabsrc,std::shared_ptr<Game> g);
  /// @brief Open a new game that uses game @a g
  void NewGame(std::shared_ptr<Game> g);
  /// @brief Singleton to get the opening book (see Openings)
  Openings& GetBook();
};

wxDECLARE_APP(MyApp);

///@brief Abort OChess with a message
void Abort(std::string msg);
