#include "ochess.hpp"
#include <wx/combobox.h>
#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/preferences.h>
#include <wx/spinctrl.h>
#include <wx/stdpaths.h>
#ifdef __APPLE__
#include <wx/bmpbndl.h>
#endif
/**
 * @brief Configuration page for the EditorCanvas
 * 
 */
class EditorPrefsPanel : public PrefsEditor {

public:
  EditorPrefsPanel(wxWindow *parent) : PrefsEditor(parent) {
   
   // Bind(wxEVT_SPINCTRL, &BoardPrefsPanel::OnConfChange, this, wxID_ANY);
  }
  void OnConfChange(wxCommandEvent &event) {
    UNUSED(event);
  }

  virtual bool TransferDataToWindow() {
    CONFIG_OPEN(config);
    row_size->SetValue(config->Read("editor/row_size", 50));
    col_size->SetValue(config->Read("editor/col_size", 100));
    show_move_icons->SetValue(config->Read("editor/show_move_icons", true));
    color_margin->SetColour(wxColour(config->Read("editor/color_margin", "#F3F3F3")));
    color_scrollbar->SetColour(wxColour(config->Read("editor/color_scrollbar", "#838383")));
    color_scrollbarbg->SetColour(wxColour(config->Read("editor/color_scrollbarbg", "#FFFFFF")));
    color_commentbg->SetColour(wxColour(config->Read("editor/color_commentbg", "#ffffcc")));
    CONFIG_CLOSE(config);
    return true;
  }

  void ApplyPreferences() {
    CONFIG_OPEN(config);
    config->Write("editor/row_size", row_size->GetValue());
    config->Write("editor/col_size", col_size->GetValue());
    config->Write("editor/show_move_icons", show_move_icons->GetValue());
    config->Write("editor/color_margin", color_margin->GetColour().GetAsString());
    config->Write("editor/color_scrollbar", color_scrollbar->GetColour().GetAsString());
    config->Write("editor/color_scrollbarbg", color_scrollbarbg->GetColour().GetAsString());
    config->Write("editor/color_commentbg", color_commentbg->GetColour().GetAsString());
    CONFIG_CLOSE(config);
  }

  virtual bool TransferDataFromWindow() {
    ApplyPreferences();
    MAINWIN->ApplyPreferences();
    return (true);
  }
};

/**
 * @brief Interface for wxWidgets to load EditorPrefsPanel into the preference window
 * 
 */
class EditorPrefs : public wxPreferencesPage {
public:
  virtual wxString GetName() const { return "Editor"; }
  virtual wxBitmap GetLargeIcon() {
    return wxArtProvider::GetBitmap(wxART_HELP, wxART_TOOLBAR);
  }
  #ifdef __APPLE__
  virtual wxBitmapBundle GetIcon() {
    return wxBitmapBundle::FromBitmap(LoadPNG("ui_text_alt"));
  }
  #endif
  virtual wxWindow *CreateWindow(wxWindow *parent) {
    return new EditorPrefsPanel(parent);
  }
};