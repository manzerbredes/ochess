#include "game_tab/left_panel/board/BoardCanvas.hpp"
#include "ochess.hpp"
#include <wx/combobox.h>
#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/preferences.h>
#include <wx/spinctrl.h>
#include <wx/stdpaths.h>
#ifdef __APPLE__
#include <wx/bmpbndl.h>
#endif
/**
 * @brief Configuration page for the BoardCanvas
 * 
 */
class BoardPrefsPanel : public PrefsBoard {
  BoardCanvas *real_board_canvas;
  wxFileName pieces_path;
  wxFileName squares_path;

public:
  BoardPrefsPanel(wxWindow *parent) : PrefsBoard(parent) {
    wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);
    real_board_canvas = new BoardCanvas((wxFrame *)board_canvas, 40, true);
    sizer->Add(real_board_canvas, 1, wxEXPAND, 5);
    board_canvas->SetSizerAndFit(sizer);

    wxStandardPaths p = wxStandardPaths::Get();
    pieces_path = wxFileName(p.GetExecutablePath());
    pieces_path = pieces_path.GetPath() + "/assets/pieces";
    squares_path = wxFileName(pieces_path);
    squares_path = squares_path.GetPath() + "/boards";
    wxLogDebug(squares_path.GetFullPath());

    Bind(wxEVT_LISTBOX, &BoardPrefsPanel::OnConfChange, this, wxID_ANY);
    Bind(wxEVT_SPINCTRL, &BoardPrefsPanel::OnConfChange, this, wxID_ANY);
  }
  void OnConfChange(wxCommandEvent &event) {
    UNUSED(event);
    ApplyPreferences();
    real_board_canvas->ApplyPreferences();
  }

  virtual bool TransferDataToWindow() {
    wxLogDebug("Load!");

    wxDir pieces_dir(pieces_path.GetFullPath());
    wxString filename;
    bool cont = pieces_dir.GetFirst(&filename, wxEmptyString, wxDIR_DEFAULT);
    piece_theme->Append("default");
    while (cont) {
      wxFileName fn(filename);
      fn.ClearExt();
      piece_theme->Append(fn.GetName());
      cont = pieces_dir.GetNext(&filename);
    }

    wxDir squares_dir(squares_path.GetFullPath());
    cont = squares_dir.GetFirst(&filename, wxEmptyString, wxDIR_DEFAULT);
    square_theme->Append("default");
    while (cont) {
      wxFileName fn(filename);
      fn.ClearExt();
      square_theme->Append(fn.GetName());
      cont = squares_dir.GetNext(&filename);
    }

    CONFIG_OPEN(config);
    piece_theme->SetStringSelection(
        config->Read("board/theme/pieces/name", "default"));
    square_theme->SetStringSelection(
        config->Read("board/theme/squares/name", "default"));
    show_side_badge->SetValue(config->Read("board/show_side_badge", true));
    show_captures->SetValue(config->Read("board/show_captures", true));
    black_by_default->SetValue(config->Read("board/black_by_default", false));
    corner_radius->SetValue(config->Read("board/corner_radius", 8));
    square_size->SetValue(config->Read("board/square_size", 80));
    CONFIG_CLOSE(config);

    return true;
  }

  void ApplyPreferences() {
    CONFIG_OPEN(config);
    wxString cur_theme = piece_theme->GetString(piece_theme->GetSelection());
    config->Write("board/theme/pieces/name", cur_theme);
    config->Write("board/theme/pieces/path",
                  pieces_path.GetFullPath() + "/" + cur_theme + ".png");
    cur_theme = square_theme->GetString(square_theme->GetSelection());
    config->Write("board/theme/squares/name", cur_theme);
    config->Write("board/theme/squares/path",
                  squares_path.GetFullPath() + "/" + cur_theme + ".png");

    config->Write("board/show_side_badge", show_side_badge->GetValue());
    config->Write("board/show_captures", show_captures->GetValue());
    config->Write("board/black_by_default", black_by_default->GetValue());
    config->Write("board/corner_radius", corner_radius->GetValue());
    config->Write("board/square_size", square_size->GetValue());

    CONFIG_CLOSE(config);
  }

  virtual bool TransferDataFromWindow() {
    ApplyPreferences();
    MAINWIN->ApplyPreferences();
    return (true);
  }
};

/**
 * @brief Interface for wxWidgets to load BoardPrefsPanel into the preference window
 * 
 */
class BoardPrefs : public wxPreferencesPage {
public:
  virtual wxString GetName() const { return "Board"; }
  virtual wxBitmap GetLargeIcon() {
    return wxArtProvider::GetBitmap(wxART_HELP, wxART_TOOLBAR);
  }
  #ifdef __APPLE__
  virtual wxBitmapBundle GetIcon() {
    return wxBitmapBundle::FromBitmap(LoadPNG("ui_rook"));
  }
  #endif
  virtual wxWindow *CreateWindow(wxWindow *parent) {
    return new BoardPrefsPanel(parent);
  }
};