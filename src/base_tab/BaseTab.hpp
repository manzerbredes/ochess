#pragma once

#include "gamebase/GameBase.hpp"
#include "ochess.hpp"
#include "BaseGameTab.hpp"
#include "BaseImportTab.hpp"
#include "BaseManageTab.hpp"

// Foreign events
wxDECLARE_EVENT(NEW_GAME_EVENT, wxCommandEvent);
wxDECLARE_EVENT(CLOSE_LINKED_TAB, wxCommandEvent);

// Local events
wxDECLARE_EVENT(REFRESH_MANAGE_TAB, wxCommandEvent);

/**
 * @brief Class that represents an opened chess games database in the MainWindow
 * @ingroup tabs
 */
class BaseTab : public TabBase, public TabInfos {
  /// @brief The opened database
  std::shared_ptr<GameBase> base;
  /// @brief The last opened game
  std::shared_ptr<Game> game;

  /// All sub tabs from this main table
  BaseGameTab *games_tab;
  BaseImportTab *import_tab;
  BaseManageTab *manage_tab;

  /// @brief Database file path
  std::string base_file;

  /// @brief Listen events from BaseImportTab
  void OnOpenGame(wxListEvent &event);
  /// @brief Listen events from BaseManageTab
  void OnSave(wxCommandEvent &event);
  
public:
  BaseTab(wxFrame *parent, std::string base_file);

  void Refresh();
  std::shared_ptr<Game> GetGame() { return (std::shared_ptr<Game>(game)); }
  std::shared_ptr<GameBase> GetBase() { return (std::shared_ptr<GameBase>(base)); };
};