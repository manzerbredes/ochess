#include "GameListManager.hpp"


GameListManager::GameListManager(wxListCtrl *game_list): game_counter(0), game_list(game_list) {
  game_list->InsertColumn(0, L"Id", wxLIST_FORMAT_LEFT, 50);
  game_list->InsertColumn(1, L"White", wxLIST_FORMAT_LEFT, 200);
  game_list->InsertColumn(2, L"Black", wxLIST_FORMAT_LEFT, 200);
  game_list->InsertColumn(3, L"Event", wxLIST_FORMAT_LEFT, 150);
  game_list->InsertColumn(4, L"Round", wxLIST_FORMAT_LEFT, 100);
  game_list->InsertColumn(5, L"Result", wxLIST_FORMAT_LEFT, 200);
  game_list->InsertColumn(6, L"ECO", wxLIST_FORMAT_LEFT, 200);
}

long GameListManager::AddGame(CType White,CType Black,CType Event,CType Round, CType Result, CType Eco){
  // Update rows elements
  rows.push_back({game_counter,White,Black,Event,Round,Result,Eco});
  // Display the row
  DisplayRow(game_counter);
  long id=game_counter;
  game_counter++;
  return id;
}

void GameListManager::DisplayRow(long id){
  RType row=rows[id];
  long index =
            game_list->InsertItem(game_counter, std::to_string(row.id)); // want this for col. 1
  game_list->SetItem(index, 1, row.White);
  game_list->SetItem(index, 2, row.Black);
  game_list->SetItem(index, 3, row.Event);
  game_list->SetItem(index, 4, row.Round);
  game_list->SetItem(index, 5, row.Result);
  game_list->SetItem(index, 6, row.Eco);

  if(std::find(opened_games.begin(), opened_games.end(), row.id) != opened_games.end())
    BG_OPEN(index);
  if(std::find(deleted_games.begin(), deleted_games.end(), row.id) != deleted_games.end())
    BG_DELETE(index);
  if(std::find(imported_games.begin(), imported_games.end(), row.id) != imported_games.end())
    BG_IMPORT(index);
}

void GameListManager::Clear(){
  game_list->DeleteAllItems();
  imported_games.clear();
  deleted_games.clear();
  opened_games.clear();
  rows.clear();
  game_counter=0;
}

void GameListManager::ClearDisplayedRow(){
  game_list->DeleteAllItems();
}

void GameListManager::MarkItemAsOpen(long item){
  opened_games.push_back(rows[item].id);
  BG_OPEN(item);
}

void GameListManager::MarkItemAsDeleted(long item){
  deleted_games.push_back(rows[item].id);
  BG_DELETE(item);
}

void GameListManager::MarkItemAsImported(long item){
  imported_games.push_back(rows[item].id);
  BG_IMPORT(item);
}

void GameListManager::SortBy(short col){
  ClearDisplayedRow();
  switch(col){
    case 1:
      std::sort(rows.begin(),rows.end(), [&](RType a, RType b){return(a.White < b.White);});
      break;
    case 2:
      std::sort(rows.begin(),rows.end(), [&](RType a, RType b){return(a.Black < b.Black);});
      break;
    case 3:
      std::sort(rows.begin(),rows.end(), [&](RType a, RType b){return(a.Event < b.Event);});
      break;
    case 4:
      std::sort(rows.begin(),rows.end(), [&](RType a, RType b){return(a.Round < b.Round);});
      break;
    case 5:
      std::sort(rows.begin(),rows.end(), [&](RType a, RType b){return(a.Result < b.Result);});
      break;
    case 6:
      std::sort(rows.begin(),rows.end(), [&](RType a, RType b){return(a.Eco < b.Eco);});
      break;
    default:
      std::sort(rows.begin(),rows.end(), [&](RType a, RType b){return(a.id < b.id);});
  }
  DISPLAY_ALL_ROWS();
}

std::vector<long> GameListManager::GetSelectedItems(){
  std::vector<long> items;
  long selected = -1;
  while ((selected = game_list->GetNextItem(selected, wxLIST_NEXT_ALL,
                                            wxLIST_STATE_SELECTED)) !=
         wxNOT_FOUND) {
    items.push_back(selected);
  }
  return(items);
}

long GameListManager::GetItemGameId(long item){
  wxListItem listItem;
    listItem.m_itemId = item;     // sets row
    listItem.m_col = 0;           // sets column to Id (column 0)
    game_list->GetItem(listItem); // gets item

  return std::stol(listItem.GetText().ToStdString());
}

void GameListManager::Filter(std::string terms){
  ClearDisplayedRow();
  for(std::size_t i=0;i<rows.size();i++){
    RType row=rows[i];
    if(TERMS_IN(White) ||
      TERMS_IN(Black) ||
      TERMS_IN(Event) ||
      TERMS_IN(Round) ||
      TERMS_IN(Result) ||
      TERMS_IN(Eco)){
      DisplayRow(i);
    }
  }
}

void GameListManager::ClearFilter() {
  ClearDisplayedRow();
  DISPLAY_ALL_ROWS();
}

