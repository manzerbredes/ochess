#pragma once

#include "ochess.hpp"
#include <algorithm>
#include <vector>

#define TERMS_IN(COL) (row.COL.find(terms) != std::string::npos)
#define BG_OPEN(INDEX) game_list->SetItemBackgroundColour(INDEX, *wxGREEN)
#define BG_DELETE(INDEX) game_list->SetItemBackgroundColour(INDEX, *wxRED)
#define BG_IMPORT(INDEX) game_list->SetItemBackgroundColour(INDEX, *wxBLUE)
#define DISPLAY_ALL_ROWS() {for(std::size_t i=0;i<rows.size();i++){DisplayRow(i);}}

///@brief Column content type
typedef std::string CType;

///@brief Row item content
typedef struct Item {
    long id;
    CType White;
    CType Black;
    CType Event;
    CType Round;
    CType Result;
    CType Eco;
} RType;

/**
 * @brief A helper class to manage a wxListCtrl that display games
 * 
 */
class GameListManager {
    long game_counter;
    wxListCtrl *game_list;
    std::vector<long> deleted_games, opened_games, imported_games;

    void DisplayRow(long id);
    void ClearDisplayedRow();
public:
    /// @brief Accessible outside (DO NOT MODIFY FROM OUTSIDE)
    std::vector<RType> rows;

    GameListManager(wxListCtrl *game_list);
    /// @brief Add a game to the list
    long AddGame(CType White,CType Black,CType Event,CType Round, CType Result, CType Eco);
    void MarkItemAsOpen(long item);
    void MarkItemAsDeleted(long item);
    void MarkItemAsImported(long item);
    /// @brief Clear the state of the GameListManager
    void Clear();
    /// @brief Return the id of the selected items
    std::vector<long> GetSelectedItems();
    /// @brief Get the game id from the item id
    long GetItemGameId(long item);
    /// @brief Filter the rows given terms
    void Filter(std::string terms);
    /// @brief Remove all filters
    void ClearFilter();
    /// @brief Sort items by the given column
    void SortBy(short col);
};