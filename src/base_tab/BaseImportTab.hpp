#pragma once

#include "ochess.hpp"
#include "GameListManager.hpp"
#include "game_tab/Game.hpp"
#include <vector>
#include <utility>

/**
 * @brief A BaseTab sub-tab to import games
 * 
 */
class BaseImportTab : public TabBase_TabImport {
  TabInfos *main_tab;
  std::shared_ptr<GameListManager> glm;
  
  // Import states data structures
  std::vector<std::shared_ptr<Game>> games_to_import;
  std::vector<std::string> databases_to_import;
  /// @brief Hold games for each databases
  std::unordered_map<std::string, std::unordered_map<long,std::shared_ptr<Game>>> selected_games_to_import;
  
  std::shared_ptr<GameBase> base;
  std::shared_ptr<GameBase> selected_base;

  void RefreshPendingImports();
  void OnLoad(wxCommandEvent &event);
  void OnImportGame(wxCommandEvent &event);
  void OnImportSelection(wxCommandEvent &event);
  void OnImportDatabase(wxCommandEvent &event);
  
public:
    int import_ndb, import_ngames,import_nselect;
    BaseImportTab(wxFrame *parent,  std::shared_ptr<GameBase> db, TabInfos *main_tab);
    void RefreshImportLists();
    void Reset(std::shared_ptr<GameBase> base);
    std::vector<std::shared_ptr<Game>> GetGameToImport();
    std::vector<std::string> GetDatabaseToImport() {return databases_to_import;};
};