#pragma once
#include "game_tab/Game.hpp"
#include <algorithm>
#include <vector>

/**
 * @brief Represent the interface that each database type (such as PGNGameBase) must follow
 * to be accessible in OChess.
 */
class GameBase {

public:
  virtual std::shared_ptr<Game> GetGame(std::uint32_t id) = 0;
  virtual void Save(std::vector<std::uint32_t> to_delete,
                    std::vector<std::string> databases_to_import,
                    std::vector<std::shared_ptr<Game>> games_to_import) = 0;
  virtual std::shared_ptr<Game> GetCurrentGame() = 0;
  virtual bool NextGame() = 0;
  virtual std::string GetTag(std::string tag) = 0;
  virtual void Reset() = 0;
  virtual std::string GetFormat() = 0;
  virtual std::string GetFilePath() = 0;
  /**
   * @brief Save the given base into current base format (export)
   * 
   * @param base 
   */
  virtual void Export(std::shared_ptr<GameBase> base) = 0;
  
  /**
   * @brief An additionnal static method is expected with the following signature:
   *  static void CreateDatabaseFile(std::string path);
   */
};

/**
 * @brief Open a data
 * 
 * @param dbpath 
 * @return std::shared_ptr<GameBase> 
 */
std::shared_ptr<GameBase> OpenDatabase(const std::string &dbpath, bool createIfNotExist=true);
/**
 * @brief Single game open
 * 
 * @param dbpath 
 * @param id 
 * @return std::shared_ptr<Game> 
 */
std::shared_ptr<Game> OpenGameX(const std::string &dbpath, long id);
/**
 * @brief Save a given game in a database file (append to existing games in the db)
 * 
 * @param dbpath path of the dadabase
 * @param g shared pointer to a game
 */
void SaveGame(const std::string &dbpath, std::shared_ptr<Game> g);