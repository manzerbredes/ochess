#include "GameBase.hpp"
#include "PGNGameBase.hpp"

std::shared_ptr<GameBase> OpenDatabase(const std::string &dbpath, bool createIfNotExist){
    wxFileName file(dbpath);
    wxString ext = file.GetExt().Lower();
    bool create=(createIfNotExist && !file.Exists());
    if (ext == "pgn") {
        if(create)
            PGNGameBase::CreateDatabaseFile(dbpath);
        return std::shared_ptr<GameBase>(new PGNGameBase(dbpath));
    }
    return nullptr;
}

std::shared_ptr<Game> OpenGameX(const std::string &dbpath, long id){
    std::shared_ptr<GameBase> base=OpenDatabase(dbpath);
    return base->GetGame(id);
}

void SaveGame(const std::string &dbpath, std::shared_ptr<Game> g){
    wxFileName file(dbpath);
    wxString ext = file.GetExt().Lower();
    // Create data structure
    std::vector<std::shared_ptr<Game>> new_games;
    new_games.push_back(g);
    std::vector<std::string> dummy_empty_bases;
    std::vector<std::uint32_t> dummy_empty_ignores;
    // Save the game
    if (ext == "pgn") {
        PGNGameBase::CreateDatabaseFile(dbpath); // Erase if exist
        GameBase *b=new PGNGameBase(dbpath);
        b->Save(dummy_empty_ignores,dummy_empty_bases, new_games);
    }
}