#pragma once

#include "GameBase.hpp"
#include "pgnp.hpp"

/**
 * @brief Used to open PGN files
 * 
 */
class PGNGameBase : public GameBase {
  pgnp::PGN *pgn;
  bool hasNextGame;
  std::string file;

public:
  PGNGameBase(std::string pgn_file);
  ~PGNGameBase();
  std::shared_ptr<Game> GetGame(std::uint32_t id);
  bool NextGame();
  std::shared_ptr<Game> GetCurrentGame();
  std::string GetTag(std::string tag);
  void Save(std::vector<std::uint32_t> to_delete,
            std::vector<std::string> databases_to_import,
            std::vector<std::shared_ptr<Game>> games_to_import);
  void Reset();
  void Export(std::shared_ptr<GameBase> base);
  std::string GetFormat() {return("PGN");};
  std::string GetFilePath() {return(file);};
  static std::string GetMovesPGN(HalfMove *m, bool needDots);
  static std::string GetPGN(std::shared_ptr<Game> g);
  static void CreateDatabaseFile(std::string path);
};