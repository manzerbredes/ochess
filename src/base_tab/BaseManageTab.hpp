#pragma once

#include "ochess.hpp"
#include "GameListManager.hpp"
#include "gamebase/GameBase.hpp"
#include "BaseImportTab.hpp"
#include "BaseGameTab.hpp"

/**
 * @brief A BaseTab sub-tab to manage games
 * 
 */
class BaseManageTab : public TabBase_TabManage {

    /// Never free the following pointers in that class
    std::shared_ptr<GameListManager> glm;
    std::shared_ptr<GameBase> base;

    /// Pointers for data access
    BaseImportTab *import_tab;
    BaseGameTab *games_tab;

    bool has_pending_events;

public:
    BaseManageTab(wxFrame *parent, std::shared_ptr<GameBase> db, 
    std::shared_ptr<GameListManager> glm, BaseImportTab *import_tab, BaseGameTab *games_tab);
    void RefreshInformations();
    void Reset(std::shared_ptr<GameBase> db);
    bool HasPendingEvents(){return(has_pending_events);};
};