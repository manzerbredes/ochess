
#pragma once

#include "gamebase/GameBase.hpp"
#include "gamebase/PGNGameBase.hpp"
#include "GameListManager.hpp"

/**
 * @brief A BaseTab sub-tab to list and search games
 * 
 */
class BaseGameTab : public TabBase_TabGames {
  std::shared_ptr<GameBase> base;
  void OnDelete(wxCommandEvent &event);
  void OnApplyFilter(wxCommandEvent &event);

public:
  std::shared_ptr<GameListManager> glm;
  /// @brief Old deleted games id
  std::vector<std::uint32_t> deleted;
  /// @brief Old edited game id+object
  std::unordered_map<long, std::shared_ptr<Game>> edited;

  BaseGameTab(wxFrame *parent, std::shared_ptr<GameBase> base);

  void Reset(std::shared_ptr<GameBase> base);
  std::vector<std::shared_ptr<Game>> GetEditedGames();
  std::vector<std::uint32_t> GetDeletedGameIds() {return(deleted);};
  std::shared_ptr<Game> OpenGame(long gameid, long item);
  
  std::shared_ptr<Game> GetGame() { return nullptr; }
  std::shared_ptr<GameBase> GetBase() { return (std::shared_ptr<GameBase>(base)); };
};