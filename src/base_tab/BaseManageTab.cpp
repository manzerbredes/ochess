#include "BaseManageTab.hpp"

#define ADD_INFO(text) {informations->WriteText(text);informations->WriteText("\n");}

BaseManageTab::BaseManageTab(wxFrame *parent, std::shared_ptr<GameBase> db, std::shared_ptr<GameListManager> glm, BaseImportTab *import_tab,BaseGameTab *games_tab):
TabBase_TabManage(parent), glm(glm), base(db), import_tab(import_tab), games_tab(games_tab)
{
    RefreshInformations();
    has_pending_events=false;
}

void BaseManageTab::RefreshInformations(){
    informations->Clear();
    wxFileName base_path(base->GetFilePath());
    ADD_INFO("Database Path: "+base_path.GetFullPath());
    ADD_INFO("File Size: "+base_path.GetHumanReadableSize());
    ADD_INFO("Last Modified: "+base_path.GetModificationTime().Format());
    ADD_INFO("Database Format: "+base->GetFormat());
    ADD_INFO("Total Number of Games: "+std::to_string(glm->rows.size()));
    int ngames=import_tab->import_ngames;
    int nselect=import_tab->import_nselect;
    int ndb=import_tab->import_ndb;
    int nedited=games_tab->edited.size();
    int ndeleted=games_tab->deleted.size()-nedited;
    if((ngames+nselect+ndb+nedited+ndeleted) >0){
        has_pending_events=true;
        ADD_INFO("\n---------- Pending operations ----------");
        ADD_INFO("Imports:");
        ADD_INFO("  -> "+std::to_string(ngames+nselect)+" game(s)");
        ADD_INFO("  -> "+std::to_string(ndb)+ " database(s)");
        ADD_INFO("Others:");
        ADD_INFO("  -> "+std::to_string(nedited)+" edited game(s)");
        ADD_INFO("  -> "+std::to_string(ndeleted)+" deleted game(s)");
    } else {
        has_pending_events=false;
    }
}

void BaseManageTab::Reset(std::shared_ptr<GameBase> db) {
    this->base=db;
    RefreshInformations();
}
