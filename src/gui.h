///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-88b0f50)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/statusbr.h>
#include <wx/aui/auibook.h>
#include <wx/sizer.h>
#include <wx/frame.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/stattext.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/panel.h>
#include <wx/notebook.h>
#include <wx/dialog.h>
#include <wx/button.h>
#include <wx/spinctrl.h>
#include <wx/listctrl.h>
#include <wx/listbox.h>
#include <wx/textctrl.h>
#include <wx/statline.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
#include <wx/clrpicker.h>
#include <wx/checkbox.h>
#include <wx/splitter.h>
#include <wx/toolbar.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>

///////////////////////////////////////////////////////////////////////////

#define ID_LIVE_ENGINE_DIALOG 1000
#define LIVE_ENGINE_PAUSE_BUTTON 1001
#define ID_DIALOG_CANCEL_BUTTON 1002
#define ID_DIALOG_IMPORT_BUTTON 1003
#define ENGINE_SAVE_CONF_BUTTON 1004
#define ENGINE_DELETE_CONF_BUTTON 1005
#define ZOOM_IN_BTN 1006
#define ZOOM_OUT_BTN 1007
#define SWAP_BTN 1008
#define COMMENT_INPUT_BOX 1009
#define wxID__ANY 1010
#define UPDATE_BTN 1011
#define DELETE_BTN 1012
#define LIVE_ANALYSIS_GAME_BUTTON 1013
#define ID_SEARCH_TERMS 1014
#define ID_APPLY_FILTER_BUTTON 1015
#define ID_TABGAMES_GAME_LIST 1016
#define ID_DELETE_BUTTON 1017
#define ID_IMPORT_GAME_BUTTON 1018
#define ID_LOAD_BUTTON 1019
#define ID_IMPORT_SELECTION 1020
#define ID_IMPORT_DB 1021
#define ID_SAVE_BUTTON 1022

///////////////////////////////////////////////////////////////////////////////
/// Class MainFrame
///////////////////////////////////////////////////////////////////////////////
class MainFrame : public wxFrame
{
	private:

	protected:
		wxMenuBar* menu_bar;
		wxMenu* menu_file;
		wxMenu* menu_game;
		wxMenu* menu_db;
		wxMenu* menu_engine;
		wxMenu* menu_help;
		wxStatusBar* status_bar;

	public:
		wxAuiNotebook* notebook;

		MainFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("OChess"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~MainFrame();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogAbout
///////////////////////////////////////////////////////////////////////////////
class DialogAbout : public wxDialog
{
	private:

	protected:
		wxNotebook* m_notebook3;
		wxPanel* info_panel;
		wxPanel* credits_panel;

	public:
		wxStaticBitmap* app_icon;
		wxStaticText* appname_text;
		wxRichTextCtrl* info_richtext;
		wxRichTextCtrl* credits_richtext;

		DialogAbout( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 600,400 ), long style = wxDEFAULT_DIALOG_STYLE );

		~DialogAbout();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogLiveEngine
///////////////////////////////////////////////////////////////////////////////
class DialogLiveEngine : public wxDialog
{
	private:

	protected:
		wxStaticText* current_engine_label;
		wxStaticText* current_engine;
		wxButton* engine_stop_button;
		wxStaticText* multipv_label;
		wxSpinCtrl* multipv;
		wxStaticText* threads_label;
		wxStaticText* threads;
		wxStaticText* m_staticText13;
		wxSpinCtrl* depth;
		wxListCtrl* lines_list;

	public:

		DialogLiveEngine( wxWindow* parent, wxWindowID id = ID_LIVE_ENGINE_DIALOG, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE );

		~DialogLiveEngine();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogAppendGame
///////////////////////////////////////////////////////////////////////////////
class DialogAppendGame : public wxDialog
{
	private:

	protected:
		wxStaticText* main_label;
		wxListBox* game_list;
		wxButton* cancel_button;
		wxButton* import_button;

	public:

		DialogAppendGame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 388,263 ), long style = wxDEFAULT_DIALOG_STYLE );

		~DialogAppendGame();

};

///////////////////////////////////////////////////////////////////////////////
/// Class TabEngine
///////////////////////////////////////////////////////////////////////////////
class TabEngine : public wxPanel
{
	private:

	protected:
		wxStaticText* engine_name_label;
		wxTextCtrl* engine_name;
		wxStaticText* engine_location_label;
		wxTextCtrl* engine_location;
		wxStaticLine* separator_1;
		wxStaticText* params_label;
		wxPropertyGrid* engine_parameters;
		wxButton* save_button;
		wxButton* delete_button;

	public:

		TabEngine( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~TabEngine();

};

///////////////////////////////////////////////////////////////////////////////
/// Class PrefsEditor
///////////////////////////////////////////////////////////////////////////////
class PrefsEditor : public wxPanel
{
	private:

	protected:
		wxStaticText* color_margin_label;
		wxColourPickerCtrl* color_margin;
		wxStaticText* color_scrollbar_label;
		wxColourPickerCtrl* color_scrollbar;
		wxStaticText* color_scrollbarbg_label;
		wxColourPickerCtrl* color_scrollbarbg;
		wxStaticText* color_commentbg_label;
		wxColourPickerCtrl* color_commentbg;
		wxStaticText* row_size_label;
		wxSpinCtrl* row_size;
		wxStaticText* col_size_label;
		wxSpinCtrl* col_size;
		wxCheckBox* show_move_icons;

	public:

		PrefsEditor( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~PrefsEditor();

};

///////////////////////////////////////////////////////////////////////////////
/// Class PrefsBoard
///////////////////////////////////////////////////////////////////////////////
class PrefsBoard : public wxPanel
{
	private:

	protected:
		wxSplitterWindow* splitter;
		wxPanel* board_canvas;
		wxPanel* options_panel;
		wxStaticText* piece_theme_label;
		wxListBox* piece_theme;
		wxStaticText* square_theme_label;
		wxListBox* square_theme;
		wxCheckBox* show_side_badge;
		wxCheckBox* show_captures;
		wxCheckBox* black_by_default;
		wxStaticText* border_radius_label;
		wxSpinCtrl* corner_radius;
		wxStaticText* board_size_label;
		wxSpinCtrl* square_size;

	public:

		PrefsBoard( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 756,751 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~PrefsBoard();

		void splitterOnIdle( wxIdleEvent& )
		{
			splitter->SetSashPosition( 350 );
			splitter->Disconnect( wxEVT_IDLE, wxIdleEventHandler( PrefsBoard::splitterOnIdle ), NULL, this );
		}

};

///////////////////////////////////////////////////////////////////////////////
/// Class TabBase
///////////////////////////////////////////////////////////////////////////////
class TabBase : public wxPanel
{
	private:

	protected:
		wxNotebook* notebook;

	public:

		TabBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 884,624 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~TabBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class TabGameLeftPanel
///////////////////////////////////////////////////////////////////////////////
class TabGameLeftPanel : public wxPanel
{
	private:

	protected:
		wxBoxSizer* main_sizer;
		wxToolBar* game_toolbar;
		wxBitmapButton* zoomin_button;
		wxBitmapButton* zoomout_button;
		wxBitmapButton* swap_button;
		wxTextCtrl* fen_text_field;

	public:

		TabGameLeftPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 998,410 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~TabGameLeftPanel();

};

///////////////////////////////////////////////////////////////////////////////
/// Class TabGameRightPanel
///////////////////////////////////////////////////////////////////////////////
class TabGameRightPanel : public wxPanel
{
	private:

	protected:
		wxNotebook* notebook;
		wxPanel* editor_page;
		wxBoxSizer* editor_page_sizer;
		wxTextCtrl* opening_label;
		wxBoxSizer* editor_canvas_sizer;
		wxStaticLine* m_staticline1;
		wxStaticText* comment_label;
		wxTextCtrl* comment_input;
		wxPanel* nag_panel;
		wxButton* nag_1;
		wxButton* nag_2;
		wxButton* nag_3;
		wxButton* nag_4;
		wxButton* nag_5;
		wxButton* nag_6;
		wxButton* nag_10;
		wxButton* nag_18;
		wxButton* nag_19;
		wxButton* nag_del;
		wxPanel* tags_page;
		wxTextCtrl* tagTextCtrl;
		wxTextCtrl* valueTextCtrl;
		wxButton* update_button;
		wxListCtrl* tags_list;
		wxButton* delete_button;
		wxPanel* engine_page;
		wxStaticText* engine_list_label;
		wxListBox* engine_list;
		wxButton* analyze_game_button;
		wxButton* live_analysis_button;

	public:

		TabGameRightPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,379 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~TabGameRightPanel();

};

///////////////////////////////////////////////////////////////////////////////
/// Class TabBase_TabGames
///////////////////////////////////////////////////////////////////////////////
class TabBase_TabGames : public wxPanel
{
	private:

	protected:
		wxStaticText* m_staticText28;
		wxTextCtrl* search_terms;
		wxButton* apply_filter_button;
		wxListCtrl* game_list;
		wxButton* delete_button;

	public:

		TabBase_TabGames( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 559,522 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~TabBase_TabGames();

};

///////////////////////////////////////////////////////////////////////////////
/// Class TabBase_TabImport
///////////////////////////////////////////////////////////////////////////////
class TabBase_TabImport : public wxPanel
{
	private:

	protected:
		wxTextCtrl* pending_imports;
		wxStaticText* from_game_label;
		wxComboBox* opened_game_list;
		wxButton* import_from_game_button;
		wxStaticLine* m_staticline4;
		wxStaticText* m_staticText29;
		wxComboBox* opened_db_list;
		wxButton* load_button;
		wxListCtrl* game_list;
		wxButton* import_from_db_button;
		wxButton* m_button16;

	public:

		TabBase_TabImport( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,661 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~TabBase_TabImport();

};

///////////////////////////////////////////////////////////////////////////////
/// Class TabBase_TabManage
///////////////////////////////////////////////////////////////////////////////
class TabBase_TabManage : public wxPanel
{
	private:

	protected:
		wxTextCtrl* informations;
		wxButton* save_button;

	public:

		TabBase_TabManage( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~TabBase_TabManage();

};

