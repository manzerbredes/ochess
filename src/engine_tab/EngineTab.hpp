#include "UCI.hpp"
#include "ochess.hpp"

// Foreign event
wxDECLARE_EVENT(CLOSE_TAB_EVENT, wxCommandEvent);
wxDECLARE_EVENT(REFRESH_ENGINE_LIST, wxCommandEvent);
wxDECLARE_EVENT(REFRESH_TAB_TITLE, wxCommandEvent);

/**
 * @brief Tab used to configure UCI chess engines
 * @ingroup tabs
 */
class EngineTab : public TabEngine, public TabInfos {
  std::string confGroup, enginePath;
  uciadapter::UCI *engine;
  std::uint32_t engine_id;
  void InitConfiguration();
  void LoadConfiguration();
  void RefreshItemList();
  void OnSave(wxCommandEvent &event);
  void OnDelete(wxCommandEvent &event);

public:
  EngineTab(wxWindow *parent, uciadapter::UCI *engine,
            std::string engine_path);
  EngineTab(wxWindow *parent, std::uint32_t id);
  ~EngineTab();
  void ApplyPreferences() {}
  std::shared_ptr<Game> GetGame() { return nullptr; }
  std::shared_ptr<GameBase> GetBase() { return nullptr; }
  std::uint32_t GetEngineId() { return engine_id; };
};