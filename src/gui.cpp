///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-88b0f50)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "gui.h"

///////////////////////////////////////////////////////////////////////////

MainFrame::MainFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	menu_bar = new wxMenuBar( 0 );
	menu_file = new wxMenu();
	menu_bar->Append( menu_file, wxT("Files") );

	menu_game = new wxMenu();
	menu_bar->Append( menu_game, wxT("Game") );

	menu_db = new wxMenu();
	menu_bar->Append( menu_db, wxT("Database") );

	menu_engine = new wxMenu();
	menu_bar->Append( menu_engine, wxT("Engine") );

	menu_help = new wxMenu();
	menu_bar->Append( menu_help, wxT("Help") );

	this->SetMenuBar( menu_bar );

	status_bar = this->CreateStatusBar( 1, wxSTB_SIZEGRIP, wxID_ANY );
	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	notebook = new wxAuiNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_DEFAULT_STYLE );

	main_sizer->Add( notebook, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( main_sizer );
	this->Layout();

	this->Centre( wxBOTH );
}

MainFrame::~MainFrame()
{
}

DialogAbout::DialogAbout( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* top_sizer;
	top_sizer = new wxBoxSizer( wxHORIZONTAL );

	app_icon = new wxStaticBitmap( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0 );
	top_sizer->Add( app_icon, 0, wxALL, 5 );

	appname_text = new wxStaticText( this, wxID_ANY, wxT("MyLabel"), wxDefaultPosition, wxDefaultSize, 0 );
	appname_text->Wrap( -1 );
	top_sizer->Add( appname_text, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	main_sizer->Add( top_sizer, 0, wxEXPAND, 5 );

	m_notebook3 = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	info_panel = new wxPanel( m_notebook3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* info_sizer;
	info_sizer = new wxBoxSizer( wxVERTICAL );

	info_richtext = new wxRichTextCtrl( info_panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxVSCROLL|wxHSCROLL|wxNO_BORDER|wxWANTS_CHARS );
	info_sizer->Add( info_richtext, 1, wxEXPAND | wxALL, 5 );


	info_panel->SetSizer( info_sizer );
	info_panel->Layout();
	info_sizer->Fit( info_panel );
	m_notebook3->AddPage( info_panel, wxT("Info"), false );
	credits_panel = new wxPanel( m_notebook3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* credits_sizer;
	credits_sizer = new wxBoxSizer( wxVERTICAL );

	credits_richtext = new wxRichTextCtrl( credits_panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0|wxVSCROLL|wxHSCROLL|wxNO_BORDER|wxWANTS_CHARS );
	credits_sizer->Add( credits_richtext, 1, wxEXPAND | wxALL, 5 );


	credits_panel->SetSizer( credits_sizer );
	credits_panel->Layout();
	credits_sizer->Fit( credits_panel );
	m_notebook3->AddPage( credits_panel, wxT("Credits"), false );

	main_sizer->Add( m_notebook3, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( main_sizer );
	this->Layout();

	this->Centre( wxBOTH );
}

DialogAbout::~DialogAbout()
{
}

DialogLiveEngine::DialogLiveEngine( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* current_engine_sizer;
	current_engine_sizer = new wxBoxSizer( wxHORIZONTAL );

	current_engine_label = new wxStaticText( this, wxID_ANY, wxT("Current engine:"), wxDefaultPosition, wxDefaultSize, 0 );
	current_engine_label->Wrap( -1 );
	current_engine_sizer->Add( current_engine_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	current_engine = new wxStaticText( this, wxID_ANY, wxT("???"), wxDefaultPosition, wxDefaultSize, 0 );
	current_engine->Wrap( -1 );
	current_engine_sizer->Add( current_engine, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	current_engine_sizer->Add( 0, 0, 1, wxEXPAND, 5 );

	engine_stop_button = new wxButton( this, LIVE_ENGINE_PAUSE_BUTTON, wxT("Stop"), wxDefaultPosition, wxDefaultSize, 0 );
	current_engine_sizer->Add( engine_stop_button, 0, wxALL, 5 );


	main_sizer->Add( current_engine_sizer, 0, wxEXPAND, 5 );

	wxBoxSizer* infos_sizer;
	infos_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* infos_sizer_row1;
	infos_sizer_row1 = new wxBoxSizer( wxHORIZONTAL );

	multipv_label = new wxStaticText( this, wxID_ANY, wxT("MultiPV:"), wxDefaultPosition, wxDefaultSize, 0 );
	multipv_label->Wrap( -1 );
	infos_sizer_row1->Add( multipv_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	multipv = new wxSpinCtrl( this, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 1 );
	infos_sizer_row1->Add( multipv, 0, wxALL, 5 );

	threads_label = new wxStaticText( this, wxID_ANY, wxT("Threads:"), wxDefaultPosition, wxDefaultSize, 0 );
	threads_label->Wrap( -1 );
	infos_sizer_row1->Add( threads_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	threads = new wxStaticText( this, wxID_ANY, wxT("?"), wxDefaultPosition, wxDefaultSize, 0 );
	threads->Wrap( -1 );
	infos_sizer_row1->Add( threads, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_staticText13 = new wxStaticText( this, wxID_ANY, wxT("Depth:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	infos_sizer_row1->Add( m_staticText13, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	depth = new wxSpinCtrl( this, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 10000, 1 );
	depth->Enable( false );

	infos_sizer_row1->Add( depth, 0, wxALL, 5 );


	infos_sizer->Add( infos_sizer_row1, 0, wxEXPAND, 5 );


	main_sizer->Add( infos_sizer, 0, wxEXPAND, 5 );

	lines_list = new wxListCtrl( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	main_sizer->Add( lines_list, 1, wxALL|wxEXPAND, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
	main_sizer->Fit( this );

	this->Centre( wxBOTH );
}

DialogLiveEngine::~DialogLiveEngine()
{
}

DialogAppendGame::DialogAppendGame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	main_label = new wxStaticText( this, wxID_ANY, wxT("Please select games to import:"), wxDefaultPosition, wxDefaultSize, 0 );
	main_label->Wrap( -1 );
	main_sizer->Add( main_label, 0, wxALL|wxEXPAND, 5 );

	game_list = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	main_sizer->Add( game_list, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxHORIZONTAL );


	bSizer5->Add( 0, 0, 1, wxEXPAND, 5 );

	cancel_button = new wxButton( this, ID_DIALOG_CANCEL_BUTTON, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer5->Add( cancel_button, 0, wxALL, 5 );

	import_button = new wxButton( this, ID_DIALOG_IMPORT_BUTTON, wxT("Import"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer5->Add( import_button, 0, wxALL, 5 );


	main_sizer->Add( bSizer5, 0, wxEXPAND, 5 );


	this->SetSizer( main_sizer );
	this->Layout();

	this->Centre( wxBOTH );
}

DialogAppendGame::~DialogAppendGame()
{
}

TabEngine::TabEngine( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* engine_name_sizer;
	engine_name_sizer = new wxBoxSizer( wxHORIZONTAL );

	engine_name_label = new wxStaticText( this, wxID_ANY, wxT("Name:"), wxDefaultPosition, wxDefaultSize, 0 );
	engine_name_label->Wrap( -1 );
	engine_name_sizer->Add( engine_name_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	engine_name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	engine_name_sizer->Add( engine_name, 1, wxALL, 5 );


	main_sizer->Add( engine_name_sizer, 0, wxEXPAND, 5 );

	wxBoxSizer* engine_location_sizer;
	engine_location_sizer = new wxBoxSizer( wxHORIZONTAL );

	engine_location_label = new wxStaticText( this, wxID_ANY, wxT("Location:"), wxDefaultPosition, wxDefaultSize, 0 );
	engine_location_label->Wrap( -1 );
	engine_location_sizer->Add( engine_location_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	engine_location = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	engine_location_sizer->Add( engine_location, 1, wxALL, 5 );


	main_sizer->Add( engine_location_sizer, 0, wxEXPAND, 5 );

	separator_1 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	main_sizer->Add( separator_1, 0, wxEXPAND | wxALL, 5 );

	params_label = new wxStaticText( this, wxID_ANY, wxT("Parameters:"), wxDefaultPosition, wxDefaultSize, 0 );
	params_label->Wrap( -1 );
	main_sizer->Add( params_label, 0, wxALL, 5 );

	engine_parameters = new wxPropertyGrid(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxPG_DEFAULT_STYLE|wxPG_SPLITTER_AUTO_CENTER);
	main_sizer->Add( engine_parameters, 1, wxALL|wxEXPAND, 5 );

	save_button = new wxButton( this, ENGINE_SAVE_CONF_BUTTON, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0 );
	main_sizer->Add( save_button, 0, wxALL|wxEXPAND, 5 );

	delete_button = new wxButton( this, ENGINE_DELETE_CONF_BUTTON, wxT("Delete engine"), wxDefaultPosition, wxDefaultSize, 0 );
	main_sizer->Add( delete_button, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
}

TabEngine::~TabEngine()
{
}

PrefsEditor::PrefsEditor( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxGridSizer* main_sizer;
	main_sizer = new wxGridSizer( 10, 2, 0, 0 );

	color_margin_label = new wxStaticText( this, wxID_ANY, wxT("Margin color"), wxDefaultPosition, wxDefaultSize, 0 );
	color_margin_label->Wrap( -1 );
	main_sizer->Add( color_margin_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	color_margin = new wxColourPickerCtrl( this, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	main_sizer->Add( color_margin, 0, wxALL, 5 );

	color_scrollbar_label = new wxStaticText( this, wxID_ANY, wxT("Scrollbar color"), wxDefaultPosition, wxDefaultSize, 0 );
	color_scrollbar_label->Wrap( -1 );
	main_sizer->Add( color_scrollbar_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	color_scrollbar = new wxColourPickerCtrl( this, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	main_sizer->Add( color_scrollbar, 0, wxALL, 5 );

	color_scrollbarbg_label = new wxStaticText( this, wxID_ANY, wxT("Scrollbar background color"), wxDefaultPosition, wxDefaultSize, 0 );
	color_scrollbarbg_label->Wrap( -1 );
	main_sizer->Add( color_scrollbarbg_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	color_scrollbarbg = new wxColourPickerCtrl( this, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	main_sizer->Add( color_scrollbarbg, 0, wxALL, 5 );

	color_commentbg_label = new wxStaticText( this, wxID_ANY, wxT("Comments background color"), wxDefaultPosition, wxDefaultSize, 0 );
	color_commentbg_label->Wrap( -1 );
	main_sizer->Add( color_commentbg_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	color_commentbg = new wxColourPickerCtrl( this, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	main_sizer->Add( color_commentbg, 0, wxALL, 5 );

	row_size_label = new wxStaticText( this, wxID_ANY, wxT("Move row size"), wxDefaultPosition, wxDefaultSize, 0 );
	row_size_label->Wrap( -1 );
	main_sizer->Add( row_size_label, 0, wxALL, 5 );

	row_size = new wxSpinCtrl( this, wxID_ANY, wxT("50"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 30, 70, 0 );
	main_sizer->Add( row_size, 0, wxALL, 5 );

	col_size_label = new wxStaticText( this, wxID_ANY, wxT("Move column size"), wxDefaultPosition, wxDefaultSize, 0 );
	col_size_label->Wrap( -1 );
	main_sizer->Add( col_size_label, 0, wxALL, 5 );

	col_size = new wxSpinCtrl( this, wxID_ANY, wxT("100"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 80, 120, 0 );
	main_sizer->Add( col_size, 0, wxALL, 5 );

	show_move_icons = new wxCheckBox( this, wxID_ANY, wxT("Show move icons"), wxDefaultPosition, wxDefaultSize, 0 );
	main_sizer->Add( show_move_icons, 0, wxALL, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
	main_sizer->Fit( this );
}

PrefsEditor::~PrefsEditor()
{
}

PrefsBoard::PrefsBoard( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	splitter = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D|wxSP_LIVE_UPDATE );
	splitter->Connect( wxEVT_IDLE, wxIdleEventHandler( PrefsBoard::splitterOnIdle ), NULL, this );

	board_canvas = new wxPanel( splitter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	options_panel = new wxPanel( splitter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* options_sizer;
	options_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* themes_sizer;
	themes_sizer = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* piece_theme_sizer;
	piece_theme_sizer = new wxBoxSizer( wxVERTICAL );

	piece_theme_label = new wxStaticText( options_panel, wxID_ANY, wxT("Piece theme"), wxDefaultPosition, wxDefaultSize, 0 );
	piece_theme_label->Wrap( -1 );
	piece_theme_sizer->Add( piece_theme_label, 0, wxALL, 5 );

	piece_theme = new wxListBox( options_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	piece_theme_sizer->Add( piece_theme, 1, wxALL|wxEXPAND, 5 );


	themes_sizer->Add( piece_theme_sizer, 1, wxEXPAND, 5 );

	wxBoxSizer* square_theme_sizer;
	square_theme_sizer = new wxBoxSizer( wxVERTICAL );

	square_theme_label = new wxStaticText( options_panel, wxID_ANY, wxT("Square theme"), wxDefaultPosition, wxDefaultSize, 0 );
	square_theme_label->Wrap( -1 );
	square_theme_sizer->Add( square_theme_label, 0, wxALL, 5 );

	square_theme = new wxListBox( options_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	square_theme_sizer->Add( square_theme, 1, wxALL|wxEXPAND, 5 );


	themes_sizer->Add( square_theme_sizer, 1, wxEXPAND, 5 );


	options_sizer->Add( themes_sizer, 1, wxEXPAND, 5 );

	show_side_badge = new wxCheckBox( options_panel, wxID_ANY, wxT("Side to play badge"), wxDefaultPosition, wxDefaultSize, 0 );
	options_sizer->Add( show_side_badge, 0, wxALL, 5 );

	show_captures = new wxCheckBox( options_panel, wxID_ANY, wxT("Show captured pieces"), wxDefaultPosition, wxDefaultSize, 0 );
	options_sizer->Add( show_captures, 0, wxALL, 5 );

	black_by_default = new wxCheckBox( options_panel, wxID_ANY, wxT("Black side by default"), wxDefaultPosition, wxDefaultSize, 0 );
	options_sizer->Add( black_by_default, 0, wxALL, 5 );

	wxBoxSizer* border_radius_sizer;
	border_radius_sizer = new wxBoxSizer( wxHORIZONTAL );

	border_radius_label = new wxStaticText( options_panel, wxID_ANY, wxT("Corner radius:"), wxDefaultPosition, wxDefaultSize, 0 );
	border_radius_label->Wrap( -1 );
	border_radius_sizer->Add( border_radius_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	corner_radius = new wxSpinCtrl( options_panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 20, 0 );
	border_radius_sizer->Add( corner_radius, 0, wxALL, 5 );


	options_sizer->Add( border_radius_sizer, 0, wxEXPAND, 5 );

	wxBoxSizer* board_size_sizer;
	board_size_sizer = new wxBoxSizer( wxHORIZONTAL );

	board_size_label = new wxStaticText( options_panel, wxID_ANY, wxT("Board squares size:"), wxDefaultPosition, wxDefaultSize, 0 );
	board_size_label->Wrap( -1 );
	board_size_sizer->Add( board_size_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	square_size = new wxSpinCtrl( options_panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 20, 150, 0 );
	board_size_sizer->Add( square_size, 0, wxALL|wxEXPAND, 5 );


	options_sizer->Add( board_size_sizer, 0, wxEXPAND, 5 );


	options_panel->SetSizer( options_sizer );
	options_panel->Layout();
	options_sizer->Fit( options_panel );
	splitter->SplitHorizontally( board_canvas, options_panel, 350 );
	main_sizer->Add( splitter, 1, wxEXPAND, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
}

PrefsBoard::~PrefsBoard()
{
}

TabBase::TabBase( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	notebook = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	main_sizer->Add( notebook, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
}

TabBase::~TabBase()
{
}

TabGameLeftPanel::TabGameLeftPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	main_sizer = new wxBoxSizer( wxVERTICAL );

	game_toolbar = new wxToolBar( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL );
	game_toolbar->Realize();

	main_sizer->Add( game_toolbar, 0, wxEXPAND, 5 );

	wxBoxSizer* bar_sizer;
	bar_sizer = new wxBoxSizer( wxHORIZONTAL );

	zoomin_button = new wxBitmapButton( this, ZOOM_IN_BTN, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );
	bar_sizer->Add( zoomin_button, 0, wxALL|wxEXPAND, 5 );

	zoomout_button = new wxBitmapButton( this, ZOOM_OUT_BTN, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );
	bar_sizer->Add( zoomout_button, 0, wxALL|wxEXPAND, 5 );

	swap_button = new wxBitmapButton( this, SWAP_BTN, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );
	bar_sizer->Add( swap_button, 0, wxALL|wxEXPAND, 5 );

	fen_text_field = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), wxTE_READONLY );
	bar_sizer->Add( fen_text_field, 1, wxALL|wxEXPAND, 0 );


	main_sizer->Add( bar_sizer, 0, wxEXPAND, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
}

TabGameLeftPanel::~TabGameLeftPanel()
{
}

TabGameRightPanel::TabGameRightPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	notebook = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	editor_page = new wxPanel( notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	editor_page_sizer = new wxBoxSizer( wxVERTICAL );

	opening_label = new wxTextCtrl( editor_page, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	editor_page_sizer->Add( opening_label, 0, wxALL|wxEXPAND, 5 );

	editor_canvas_sizer = new wxBoxSizer( wxVERTICAL );


	editor_page_sizer->Add( editor_canvas_sizer, 100, wxEXPAND, 5 );

	m_staticline1 = new wxStaticLine( editor_page, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	editor_page_sizer->Add( m_staticline1, 0, wxEXPAND | wxALL, 5 );

	comment_label = new wxStaticText( editor_page, wxID_ANY, wxT("Comment:"), wxDefaultPosition, wxDefaultSize, 0 );
	comment_label->Wrap( -1 );
	editor_page_sizer->Add( comment_label, 0, wxALL|wxEXPAND, 5 );

	comment_input = new wxTextCtrl( editor_page, COMMENT_INPUT_BOX, wxEmptyString, wxDefaultPosition, wxSize( -1,200 ), wxTE_MULTILINE );
	editor_page_sizer->Add( comment_input, 0, wxALL|wxEXPAND, 5 );

	nag_panel = new wxPanel( editor_page, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridSizer* nag_sizer;
	nag_sizer = new wxGridSizer( 2, 8, 0, 0 );

	nag_1 = new wxButton( nag_panel, wxID__ANY, wxT("!"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_1, 0, wxALL|wxEXPAND, 5 );

	nag_2 = new wxButton( nag_panel, wxID_ANY, wxT("?"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_2, 0, wxALL|wxEXPAND, 5 );

	nag_3 = new wxButton( nag_panel, wxID_ANY, wxT("!!"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_3, 0, wxALL|wxEXPAND, 5 );

	nag_4 = new wxButton( nag_panel, wxID_ANY, wxT("??"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_4, 0, wxALL|wxEXPAND, 5 );

	nag_5 = new wxButton( nag_panel, wxID_ANY, wxT("!?"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_5, 0, wxALL|wxEXPAND, 5 );

	nag_6 = new wxButton( nag_panel, wxID_ANY, wxT("?!"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_6, 0, wxALL|wxEXPAND, 5 );

	nag_10 = new wxButton( nag_panel, wxID_ANY, wxT("="), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_10, 0, wxALL|wxEXPAND, 5 );

	nag_18 = new wxButton( nag_panel, wxID_ANY, wxT("+-"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_18, 0, wxALL|wxEXPAND, 5 );

	nag_19 = new wxButton( nag_panel, wxID_ANY, wxT("-+"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_19, 0, wxALL|wxEXPAND, 5 );

	nag_del = new wxButton( nag_panel, wxID_ANY, wxT("DEL"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	nag_sizer->Add( nag_del, 0, wxALL|wxEXPAND, 5 );


	nag_panel->SetSizer( nag_sizer );
	nag_panel->Layout();
	nag_sizer->Fit( nag_panel );
	editor_page_sizer->Add( nag_panel, 1, wxEXPAND | wxALL, 5 );


	editor_page->SetSizer( editor_page_sizer );
	editor_page->Layout();
	editor_page_sizer->Fit( editor_page );
	notebook->AddPage( editor_page, wxT("Editor"), false );
	tags_page = new wxPanel( notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* tags_page_sizer;
	tags_page_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* tags_list_control_sizer;
	tags_list_control_sizer = new wxBoxSizer( wxVERTICAL );

	tagTextCtrl = new wxTextCtrl( tags_page, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	tags_list_control_sizer->Add( tagTextCtrl, 0, wxALL|wxEXPAND, 5 );

	valueTextCtrl = new wxTextCtrl( tags_page, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	tags_list_control_sizer->Add( valueTextCtrl, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );

	update_button = new wxButton( tags_page, UPDATE_BTN, wxT("Update"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( update_button, 1, wxALL, 5 );


	tags_list_control_sizer->Add( bSizer7, 1, wxEXPAND, 5 );


	tags_page_sizer->Add( tags_list_control_sizer, 0, wxEXPAND, 5 );

	tags_list = new wxListCtrl( tags_page, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	tags_page_sizer->Add( tags_list, 1, wxALL|wxEXPAND, 5 );

	delete_button = new wxButton( tags_page, DELETE_BTN, wxT("Delete selection"), wxDefaultPosition, wxDefaultSize, 0 );
	tags_page_sizer->Add( delete_button, 0, wxALL|wxEXPAND, 5 );


	tags_page->SetSizer( tags_page_sizer );
	tags_page->Layout();
	tags_page_sizer->Fit( tags_page );
	notebook->AddPage( tags_page, wxT("Tags"), false );
	engine_page = new wxPanel( notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* engine_page_sizer;
	engine_page_sizer = new wxBoxSizer( wxVERTICAL );

	engine_list_label = new wxStaticText( engine_page, wxID_ANY, wxT("Choose the engine to use:"), wxDefaultPosition, wxDefaultSize, 0 );
	engine_list_label->Wrap( -1 );
	engine_page_sizer->Add( engine_list_label, 0, wxALL, 5 );

	engine_list = new wxListBox( engine_page, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	engine_page_sizer->Add( engine_list, 1, wxALL|wxEXPAND, 5 );

	analyze_game_button = new wxButton( engine_page, wxID_ANY, wxT("Analyze game"), wxDefaultPosition, wxDefaultSize, 0 );
	engine_page_sizer->Add( analyze_game_button, 0, wxALL|wxEXPAND, 5 );

	live_analysis_button = new wxButton( engine_page, LIVE_ANALYSIS_GAME_BUTTON, wxT("Live analysis"), wxDefaultPosition, wxDefaultSize, 0 );
	engine_page_sizer->Add( live_analysis_button, 0, wxALL|wxEXPAND, 5 );


	engine_page->SetSizer( engine_page_sizer );
	engine_page->Layout();
	engine_page_sizer->Fit( engine_page );
	notebook->AddPage( engine_page, wxT("Engine"), false );

	main_sizer->Add( notebook, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
}

TabGameRightPanel::~TabGameRightPanel()
{
}

TabBase_TabGames::TabBase_TabGames( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* top_sizer;
	top_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_staticText28 = new wxStaticText( this, wxID_ANY, wxT("Filter:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText28->Wrap( -1 );
	top_sizer->Add( m_staticText28, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	search_terms = new wxTextCtrl( this, ID_SEARCH_TERMS, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	top_sizer->Add( search_terms, 1, wxALL|wxEXPAND, 5 );

	apply_filter_button = new wxButton( this, ID_APPLY_FILTER_BUTTON, wxT("Apply"), wxDefaultPosition, wxDefaultSize, 0 );
	top_sizer->Add( apply_filter_button, 0, wxALL, 5 );


	main_sizer->Add( top_sizer, 0, wxEXPAND, 5 );

	game_list = new wxListCtrl( this, ID_TABGAMES_GAME_LIST, wxDefaultPosition, wxSize( -1,-1 ), wxLC_REPORT );
	main_sizer->Add( game_list, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bottom_sizer;
	bottom_sizer = new wxBoxSizer( wxHORIZONTAL );

	delete_button = new wxButton( this, ID_DELETE_BUTTON, wxT("Mark as deleted"), wxDefaultPosition, wxDefaultSize, 0 );
	bottom_sizer->Add( delete_button, 1, wxALL|wxEXPAND, 5 );


	main_sizer->Add( bottom_sizer, 0, wxEXPAND, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
}

TabBase_TabGames::~TabBase_TabGames()
{
}

TabBase_TabImport::TabBase_TabImport( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	pending_imports = new wxTextCtrl( this, wxID_ANY, wxT("No pending imports"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	main_sizer->Add( pending_imports, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* games_sizer;
	games_sizer = new wxBoxSizer( wxHORIZONTAL );

	from_game_label = new wxStaticText( this, wxID_ANY, wxT("Games:"), wxDefaultPosition, wxDefaultSize, 0 );
	from_game_label->Wrap( -1 );
	from_game_label->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );

	games_sizer->Add( from_game_label, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	opened_game_list = new wxComboBox( this, wxID_ANY, wxT("No game opened"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	games_sizer->Add( opened_game_list, 100, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	import_from_game_button = new wxButton( this, ID_IMPORT_GAME_BUTTON, wxT("Import Game"), wxDefaultPosition, wxDefaultSize, 0 );
	games_sizer->Add( import_from_game_button, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	main_sizer->Add( games_sizer, 0, wxALIGN_TOP|wxEXPAND, 5 );

	m_staticline4 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	main_sizer->Add( m_staticline4, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* databases_sizer;
	databases_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer33;
	bSizer33 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText29 = new wxStaticText( this, wxID_ANY, wxT("Database:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText29->Wrap( -1 );
	bSizer33->Add( m_staticText29, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	opened_db_list = new wxComboBox( this, wxID_ANY, wxT("No other databases opened"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	bSizer33->Add( opened_db_list, 100, wxALL|wxEXPAND, 5 );

	load_button = new wxButton( this, ID_LOAD_BUTTON, wxT("Load"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer33->Add( load_button, 0, wxALL, 5 );


	databases_sizer->Add( bSizer33, 0, wxEXPAND, 5 );

	game_list = new wxListCtrl( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	game_list->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_INACTIVECAPTIONTEXT ) );

	databases_sizer->Add( game_list, 100, wxALL|wxEXPAND, 5 );

	import_from_db_button = new wxButton( this, ID_IMPORT_SELECTION, wxT("Import Selection"), wxDefaultPosition, wxDefaultSize, 0 );
	databases_sizer->Add( import_from_db_button, 0, wxALL|wxEXPAND, 5 );

	m_button16 = new wxButton( this, ID_IMPORT_DB, wxT("Import entire database"), wxDefaultPosition, wxDefaultSize, 0 );
	databases_sizer->Add( m_button16, 0, wxALL|wxEXPAND, 5 );


	main_sizer->Add( databases_sizer, 80, wxEXPAND, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
}

TabBase_TabImport::~TabBase_TabImport()
{
}

TabBase_TabManage::TabBase_TabManage( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* main_sizer;
	main_sizer = new wxBoxSizer( wxVERTICAL );

	informations = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	informations->SetBackgroundColour( wxColour( 235, 235, 235 ) );

	main_sizer->Add( informations, 100, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bottom_sizer;
	bottom_sizer = new wxBoxSizer( wxVERTICAL );

	save_button = new wxButton( this, ID_SAVE_BUTTON, wxT("Save All Changes"), wxDefaultPosition, wxDefaultSize, 0 );
	bottom_sizer->Add( save_button, 0, wxALL|wxEXPAND, 5 );


	main_sizer->Add( bottom_sizer, 1, wxEXPAND, 5 );


	this->SetSizer( main_sizer );
	this->Layout();
}

TabBase_TabManage::~TabBase_TabManage()
{
}
