#include "ochess.hpp"
#include "MainWindow.hpp"
#include "base_tab/BaseTab.hpp"

bool MyApp::OnInit() {
  wxImage::AddHandler(new wxPNGHandler);

  // Check config version
  CONFIG_OPEN(conf);
  wxString version = conf->Read("version", CONFIG_VERSION);
  conf->Write("version", version); // Setup config file version
  wxLogDebug("Starting ochess with configuration file version %s", version);
  CONFIG_CLOSE(conf);

  // Advertise for configuration file version
  if (version != CONFIG_VERSION) {
    wxMessageDialog *dial = new wxMessageDialog(
        NULL,
        wxT("Configuration files version missmatch. Expected " +
            std::string(CONFIG_VERSION) + " but got " + version +
            ".\nExisting OChess configuration must be erased.\n" +
            "Do you still want to proceed?"),
        wxT("Information"), wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION);

    if (dial->ShowModal() != wxID_YES)
      return false;
    // First remove everything:
    CONFIG_OPEN(conf2);
    conf2->DeleteAll();
    conf2->Write("version", CONFIG_VERSION);
    CONFIG_CLOSE(conf2);
    // Now Create new configuration:
    CONFIG_OPEN(conf3);
    conf3->Write("version", CONFIG_VERSION);
    CONFIG_CLOSE(conf3);
  }

  // Main frame
  MainWindow *frame = new MainWindow();
  frame->Show(true);

  return true;
}

std::vector<TabInfos *> MyApp::ListTabInfos() {
  std::vector<TabInfos *> tinfos;
  wxAuiNotebook *notebook = ((MainWindow *)this->GetTopWindow())->notebook;
  for (std::size_t i = 0; i < notebook->GetPageCount(); i++) {
    tinfos.push_back(dynamic_cast<TabInfos *>(notebook->GetPage(i)));
  }
  return (tinfos);
}

void MyApp::FocusOnTab(TabInfos * toFocus){
  wxAuiNotebook *notebook = ((MainWindow *)this->GetTopWindow())->notebook;
  for (std::size_t i = 0; i < notebook->GetPageCount(); i++) {
    if(dynamic_cast<TabInfos *>(notebook->GetPage(i))->id== toFocus->id)
      notebook->SetSelection(i);
  }
}

Openings &MyApp::GetBook() { return Book; }

void MyApp::NewGame(TabInfos *tabsrc, std::shared_ptr<Game> g) {
  MainWindow *w = ((MainWindow *)this->GetTopWindow());
  TabInfos *i = w->NewGame(g);
  i->Link(tabsrc); // Link opened game to tabsrc
}

void MyApp::NewGame(std::shared_ptr<Game> g) {
  MainWindow *w = ((MainWindow *)this->GetTopWindow());
  w->NewGame(g);
}

wxIMPLEMENT_APP(MyApp);

void Abort(std::string msg) {
  wxMessageDialog *dial = new wxMessageDialog(NULL, wxString(msg), wxT("Error"),
                                              wxOK | wxICON_ERROR);
  dial->ShowModal();
  wxLogFatalError(wxString(msg));
}

long TabInfos::tab_count = 0;
void TabInfos::Link(TabInfos *tab) {
  this->is_linked = true;
  this->linked_id = tab->id;
  this->OnLink();
}