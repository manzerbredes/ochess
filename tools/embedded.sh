#!/bin/bash
# This file generate all the embedded binary resources required for OChess to run properly

########## RESOURCES ##########
ressources=$(cat <<-EndOfResources
assets/boards/chesscom_8bits.png
assets/pieces/cburnett.png
EndOfResources
)
###############################

set -e
wai=$(dirname $(readlink -f "$0")) # Current script directory
dst="${wai}/../src/binres/binary_data.hpp"
ui_icons_width=24
ui_tmp=$(mktemp -d)

# Binary to C headers
# $1 is the resource file and $2 the variable name to use in the C code
bin2c () {
    xxd -n "$2" -i "$1"
}

# Generate ui icons png from svg
rm -f "${wai}"/assets/icons/ui/*.png # Clear previous data
for svg in $(find ${wai}/assets/icons/ui -name "*.svg")
do
    ext="${svg##*.}"
    name="$(basename $svg .${ext})"
    echo "Generating png for ${name}.${ext}..."
    inkscape --export-type png --export-filename "${ui_tmp}/ui-${name}.png" -w "${ui_icons_width}" "${svg}"
done

# Generate headers
echo -n > "$dst"
for res in $ressources $(find ${wai}/assets/icons/ -name "*.png") $(find ${ui_tmp} -name "*.png")
do
    ext="${res##*.}"
    name="$(basename $res .${ext})"
    echo "Generating resources for ${name}.${ext}..."
    bin2c "$res" "${name}_${ext}" >> "$dst"
done
rm -rf ${ui_tmp}
echo "Done! The generated resources are accessible here: \"$dst\""
