%Opening Names DB
=====

The chess opening names database uses the [Lichess chess-openings](https://github.com/lichess-org/chess-openings) project. The script `tools/openings.sh` is used to fetch the last updates from the project, and generate the `binres/openings.hpp` file. In turn, this file is used in Openings.hpp.


#### Acknowledgements

Thanks to [lichess-org](https://github.com/lichess-org) for the tremendous work, and providing the [chess-openings](https://github.com/lichess-org/chess-openings) project.
