Internal Libraries
====

Ochess uses 5 internal libraries (excluding [wxWidgets](https://www.wxwidgets.org/)):
- [cgeditor](https://gitlab.com/manzerbredes/cgeditor): A 2D chess game moves presenter/editor
- [pgnp](https://gitlab.com/manzerbredes/pgnp): An efficient PGN parser
- [chessarbiter](https://gitlab.com/manzerbredes/chessarbiter): A chess classical chess game arbiter for C++
- [uciadapter](https://gitlab.com/manzerbredes/uciadapter): A cross platform utility to interact with UCI chess engines
- [chess-move-interface](https://gitlab.com/manzerbredes/chess-move-interface): A chess half move interface for libraries interoperability

### CGEditor
It is only used in EditorCanvas.

### PGNP
It is only used in PGNGameBase.

### ChessArbiter
Mostly used in Game and HalfMove.

### UCIAdapter
Used in various engine related areas such as MainWindow, EngineTab and LiveEngineDialog.

### Chess-Move-Interface
Used in various region of OChess such as PGNGameBase and HalfMove.