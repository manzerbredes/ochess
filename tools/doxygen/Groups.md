@defgroup binres Embbeded binary resources utilities
@brief How to access to the embbeded binary data
@details Binary resources are generated into the binary_data.hpp file using
the `tools/embbeded.sh` script. Here are the various functions used 
to access to them.

@defgroup game Chess game model
@brief Classes used to model a chess game

@defgroup tabs UI tabs
@brief All classes that implement a tab visible in the main window