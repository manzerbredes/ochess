Board Theming
======

In OChess, every board skins (squares and pieces) are made of *200x200* pixels tiles stored in <i>.png</i> images. OChess is in charge of breaking these tiles apart into individual elements.

### Squares
Squares skins are <i>.png</i> images, with a dimension of *400x200* pixels (2 tiles next to each other). The tile starting at pixel (0,0) are for dark squares and the other one for light squares.

### Pieces

Pieces skins are <i>.png</i> images, with a dimension of *400x1200* pixels (2 columns of 6 tiles). The first column, starting at pixel (0,0) are for black pieces and the other column for the white pieces. From top, to bottom pieces are in the following order: King, Queen, Rook, Bishop, Knight, Pawn.

### Create a Skin with generate.sh

To create a pieces skin named *myskin*, create the folder `tools/skin/pieces/myskin`. In this folder, place all the pieces in the <i>.svg</i> format with the following name convention: `bb.svg` for <b>b</b>lack <b>b</b>ishop, `wk.svg` for <b>w</b>hite <b>k</b>ing, `wn.svg` for <b>w</b>hite <b>k</b>night, `bn.svg` for <b>b</b>lack <b>k</b>night,...

Then, run the `tools/skin/generate.sh` script. This script will combine the <i>.svg</i> file into a single <i>.png</i> file located in `tools/assets/pieces/myskin.png`.

Use the same approach to create boards skins.

**Note:** The `tools/skin/generate.sh` script is meant to be used on Linux Distributions and requires [Bash](https://www.gnu.org/software/bash/), [ImageMagick](https://imagemagick.org/) and [Inkscape](https://inkscape.org/).