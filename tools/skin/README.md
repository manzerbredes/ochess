# How skins work
Every skins are made of `200x200` square images. Thus, board skins
are `400x200` images and pieces skins are `400x1200` images.

How to make a skin ? Create a sub-directory in boards/pieces named by your skin name and put every svg files which compose your skin. Then run:

> ./generate.sh

Then your skin should appears in the assets directory.
**Warning:** *Please have consistent svg file names (see existing skins).*

Existing skins sources:
- [chesscom](https://www.chess.com/)
- [cburnett](https://commons.wikimedia.org/wiki/Category:SVG_chess_pieces)
- [mgilberto](https://svg-clipart.com/symbol/YCQnfQg-chess-set-symbols-clipart)
- [simple](https://freesvg.org/chess-pieces-vector)

