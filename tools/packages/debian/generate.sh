#!/bin/bash

archive="ochess-master.tar.bz2"
pkg="ochess"

# Build
tar -xvf $archive
old_dir=$(pwd)
cd ochess-master/
mkdir -p build && cd build
cmake ../
make
cd $old_dir

# Create pkg
mkdir -p $pkg/usr/local/bin
mkdir -p $pkg/usr/share/ochess
mkdir -p $pkg/DEBIAN

cp ochess-master/build/ochess $pkg/usr/local/bin/
cp -r ochess-master/build/assets/ $pkg/usr/share/ochess/
cp control $pkg/DEBIAN/

# Build package
dpkg-deb --build ochess
