#!/bin/bash
# OChess is using the lichess chess-openings database
# that can be found here: https://github.com/lichess-org/chess-openings

# Init variables
urldb="https://github.com/lichess-org/chess-openings"
tmp=$(mktemp -d)
wai=$(dirname $(readlink -f "$0")) # Current script directory
dst="${wai}/../src/binres/"

# Fetch database
git clone "$urldb" "$tmp"
cd $tmp

# a.tsv
echo -n "static const char a_tsv[] =" > openings.hpp
sed -i '1d' a.tsv # remove header
cat a.tsv | sed -e "s/^/\"/g" -e "s/$/\\\n\"/g" -e '$ s/\\n"/";/g' >> openings.hpp;

# b.tsv
echo -n "static const char b_tsv[] =" >> openings.hpp
sed -i '1d' b.tsv
cat b.tsv | sed -e "s/^/\"/g" -e "s/$/\\\n\"/g" -e '$ s/\\n"/";/g' >> openings.hpp;

# c.tsv
echo -n "static const char c_tsv[] =" >> openings.hpp
sed -i '1d' c.tsv
cat c.tsv | sed -e "s/^/\"/g" -e "s/$/\\\n\"/g" -e '$ s/\\n"/";/g' >> openings.hpp;

# d.tsv
echo -n "static const char d_tsv[] =" >> openings.hpp
sed -i '1d' d.tsv
cat d.tsv | sed -e "s/^/\"/g" -e "s/$/\\\n\"/g" -e '$ s/\\n"/";/g' >> openings.hpp;

# e.tsv
echo -n "static const char e_tsv[] =" >> openings.hpp
sed -i '1d' e.tsv
cat e.tsv | sed -e "s/^/\"/g" -e "s/$/\\\n\"/g" -e '$ s/\\n"/";/g' >> openings.hpp;

# Save
mv openings.hpp "$dst"

# Cleaning
rm -rf "$tmp"
