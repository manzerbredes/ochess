# Open Chess &nbsp; [![Pipeline](https://gitlab.com/manzerbredes/ochess/badges/master/pipeline.svg)](https://gitlab.com/manzerbredes/ochess/pipelines) [![Pipeline](https://img.shields.io/badge/cmake-v3.10-blue)](https://cmake.org/download/) [![Pipeline](https://img.shields.io/badge/wxWidgets-v3.0.4-blue)](https://www.wxwidgets.org/)

![Screenshot](tools/assets/icons/screenshot.jpg)

## Features
 - PGNs files (*.si4* files not yet but soon!)
 - Handle heavy game files
 - Live chess engine analysis with *UCI* compliant chess engines
 - Intuitive game annotations
 - Simple games database management system

## Installing OChess
On ArchLinux from AUR:

    > yay -S ochess

## Running OChess from sources
Requires *CMake* and *wxWidgets*:

    > git clone --recursive https://gitlab.com/manzerbredes/ochess
    > cd ./ochess
    > mkdir ./build && cd ./build
    > cmake ../ && make
    > ./ochess # Launch OChess

## Notes

- OChess is still in early development stage. For more informations, please read the `TODO.md` file.
- For questions and discussions please join [irc://irc.libera.chat/ochess](https://web.libera.chat/#ochess).

## Satellites projects
OChess is based on several satellite projects that implement individual and independant features:
 - [chessarbiter](https://gitlab.com/manzerbredes/chessarbiter): A chess classical chess game C++ arbiter
 - [pgnp](https://gitlab.com/manzerbredes/pgnp): An efficient PGN parser
 - [uciadapter](https://gitlab.com/manzerbredes/uciadapter): A cross platform utility to interact with *UCI* chess engines
 - [cgeditor](https://gitlab.com/manzerbredes/cgeditor): A 2D chess game moves presenter/editor
 - [chess-move-interface](https://gitlab.com/manzerbredes/chess-move-interface) A chess half move interface for libraries interoperability

 ## Acknowledgments
 - The [chess-openings](https://github.com/lichess-org/chess-openings) project by [lichess-org](https://github.com/lichess-org) for their openings databases